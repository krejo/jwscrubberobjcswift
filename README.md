# README #

Isolates the scrubber portion of JamWith in its own project. This repo was created from the Companion Repo JWScrubberObjC  which is all objective-C as seen from JamWith and pulled from JamWith from the moment of saved_presets

### Purpose ###

* Scrubber Controller
* Modernize the scrubber controller to break into parts. As massive view controller was too complex
* Version 1.x


### How do I get set up? ###

* Clone repo
* Open in Xcode7

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact


### Progress ###

* JWFileAnalyzer.swift

Was created to remove file analyzer code from Scrubber Controller

* JWBufferController.swift

Was created to remove buffer received code from Scrubber Controller



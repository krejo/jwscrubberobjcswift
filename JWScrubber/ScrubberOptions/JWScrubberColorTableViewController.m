//
//  JWScrubberColorTableViewController.m
//  JWScrubber
//
//  Created by JOSEPH KERR on 11/24/15.
//  Copyright © 2015 JOSEPH KERR. All rights reserved.
//

#import "JWScrubberColorTableViewController.h"

@interface JWScrubberColorTableViewController ()

@end

@implementation JWScrubberColorTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // self.clearsSelectionOnViewWillAppear = NO;
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Incomplete implementation, return the number of sections
//    return 0;
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete implementation, return the number of rows
//    return 0;

//iosColor1 = [UIColor colorWithRed:128/255.0 green:128/255.0 blue:0/255.0 alpha:1.0]; // asparagus
//iosColor2 = [UIColor colorWithRed:0/255.0 green:64/255.0 blue:128/255.0 alpha:1.0]; // ocean
//iosColor3 = [UIColor colorWithRed:0/255.0 green:128/255.0 blue:255/255.0 alpha:1.0]; // aqua
//iosColor4 = [UIColor colorWithRed:102/255.0 green:204/255.0 blue:255/255.0 alpha:1.0]; // sky

- (void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    if (indexPath.row == 0) {
        cell.textLabel.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor blackColor];
    } else if (indexPath.row == 1) {
        cell.textLabel.backgroundColor = [UIColor redColor];
        cell.textLabel.textColor = [UIColor yellowColor];
    } else if (indexPath.row == 2) {
        cell.textLabel.backgroundColor = [UIColor greenColor];
        cell.textLabel.textColor = [UIColor blackColor];
    } else if (indexPath.row == 3) {
        cell.textLabel.backgroundColor = [UIColor blueColor];
        cell.textLabel.textColor = [UIColor whiteColor];
    } else if (indexPath.row == 4) {
        cell.textLabel.backgroundColor = [UIColor blackColor];
        cell.textLabel.textColor = [UIColor whiteColor];
    } else if (indexPath.row == 5) {
        cell.textLabel.backgroundColor = [UIColor colorWithRed:128/255.0 green:128/255.0 blue:0/255.0 alpha:1.0]; // asparagus
        cell.textLabel.textColor = [UIColor yellowColor];
    }else if (indexPath.row == 6) {
        cell.textLabel.backgroundColor = [UIColor colorWithRed:0/255.0 green:64/255.0 blue:128/255.0 alpha:1.0]; // ocean
        cell.textLabel.textColor = [UIColor yellowColor];
    }else if (indexPath.row == 7) {
        cell.textLabel.backgroundColor = [UIColor colorWithRed:0/255.0 green:128/255.0 blue:255/255.0 alpha:1.0]; // aqua
        cell.textLabel.textColor = [UIColor blackColor];
    }else if (indexPath.row == 8) {
        cell.textLabel.backgroundColor = [UIColor colorWithRed:102/255.0 green:204/255.0 blue:255/255.0 alpha:1.0]; // sky
        cell.textLabel.textColor = [UIColor blackColor];
    }

}

// white red green blue black asparagus ocean aqua sky

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        _selectedColor = [UIColor whiteColor];
    } else if (indexPath.row == 1) {
        _selectedColor = [UIColor redColor];
    } else if (indexPath.row == 2) {
        _selectedColor = [UIColor greenColor];
    } else if (indexPath.row == 3) {
        _selectedColor = [UIColor blueColor];
    } else if (indexPath.row == 4) {
        _selectedColor = [UIColor blackColor];
    } else if (indexPath.row == 5) {
        _selectedColor = [UIColor colorWithRed:128/255.0 green:128/255.0 blue:0/255.0 alpha:1.0]; // asparagus
    }else if (indexPath.row == 6) {
        _selectedColor = [UIColor colorWithRed:0/255.0 green:64/255.0 blue:128/255.0 alpha:1.0]; // ocean
    }else if (indexPath.row == 7) {
        _selectedColor = [UIColor colorWithRed:0/255.0 green:128/255.0 blue:255/255.0 alpha:1.0]; // aqua
    }else if (indexPath.row == 8) {
        _selectedColor = [UIColor colorWithRed:102/255.0 green:204/255.0 blue:255/255.0 alpha:1.0]; // sky
    }
    
    [_delegate colorSelected:self];
    
}

//    if (_selected)
//        [tableView reloadRowsAtIndexPaths:@[_selected] withRowAnimation:UITableViewRowAnimationNone];
//    _selected = indexPath;
//    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    // Configure the cell...
    return cell;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

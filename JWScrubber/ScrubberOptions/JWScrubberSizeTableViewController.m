//
//  JWScrubberSizeTableViewController.m
//  JWScrubber
//
//  Created by JOSEPH KERR on 11/22/15.
//  Copyright © 2015 JOSEPH KERR. All rights reserved.
//

#import "JWScrubberSizeTableViewController.h"

@interface JWScrubberSizeTableViewController ()

@property (nonatomic) NSIndexPath *selected;

@end

//0 SampleSize4,
//  SampleSize6,
//1 SampleSize8,
//2 SampleSize10,
//3 SampleSize14,
//4 SampleSize18,
//  SampleSize24,

@implementation JWScrubberSizeTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.clearsSelectionOnViewWillAppear = NO;
    
    if (_sampleSz == SampleSize4) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                    animated:NO scrollPosition:UITableViewScrollPositionNone];
    } else if (_sampleSz == SampleSize8) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]
                                    animated:NO scrollPosition:UITableViewScrollPositionNone];
    } else if (_sampleSz == SampleSize10) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]
                                    animated:NO scrollPosition:UITableViewScrollPositionNone];
    } else if (_sampleSz == SampleSize14) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]
                                    animated:NO scrollPosition:UITableViewScrollPositionNone];
    } else if (_sampleSz == SampleSize18) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0]
                                    animated:NO scrollPosition:UITableViewScrollPositionNone];
    }

    _selected = [self.tableView indexPathForSelectedRow];
    
    if (_selected) {
        [self.tableView reloadRowsAtIndexPaths:@[_selected] withRowAnimation:UITableViewRowAnimationNone];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    cell.accessoryType = UITableViewCellAccessoryNone;

    if (indexPath.row == 0 && _sampleSz == SampleSize4) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else if (indexPath.row == 1 && _sampleSz == SampleSize8) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else if (indexPath.row == 2 && _sampleSz == SampleSize10) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else if (indexPath.row == 3 && _sampleSz == SampleSize14) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else if (indexPath.row == 4 && _sampleSz == SampleSize18) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath {

    if (indexPath.row == 0) {
        _sampleSz = SampleSize4;
    } else if (indexPath.row == 1) {
        _sampleSz = SampleSize8;
    } else if (indexPath.row == 2) {
        _sampleSz = SampleSize10;
    } else if (indexPath.row == 3) {
        _sampleSz = SampleSize14;
    } else if (indexPath.row == 4) {
        _sampleSz = SampleSize18;
    }

    if (_selected) {
        [tableView reloadRowsAtIndexPaths:@[_selected] withRowAnimation:UITableViewRowAnimationNone];
    }

    _selected = indexPath;

    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];

    [_delegate selectionSizeMade:self];
}


/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    // Configure the cell...
    return cell;
}
*/

@end

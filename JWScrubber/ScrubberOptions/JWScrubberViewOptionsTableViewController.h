//
//  JWScrubberViewOptionsTableViewController.h
//  JWScrubber
//
//  Created by JOSEPH KERR on 11/24/15.
//  Copyright © 2015 JOSEPH KERR. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JWScrubber.h"

@protocol JWScrubberViewOptionsDelegate;

@interface JWScrubberViewOptionsTableViewController : UITableViewController
@property (nonatomic) ScrubberViewOptions viewOptions;
@property (nonatomic) NSDictionary *scrubberColors;
@property (weak,nonatomic) id <JWScrubberViewOptionsDelegate> delegate;
@end

@protocol JWScrubberViewOptionsDelegate <NSObject>
-(void)selectionViewOptionsMade:(JWScrubberViewOptionsTableViewController*)controller;
-(void)willCompleteViewOptions:(JWScrubberViewOptionsTableViewController*)controller;
@end




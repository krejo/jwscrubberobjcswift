//
//  JWOptionControlsViewController.swift
//  JWScrubber
//
//  Created by JOSEPH KERR on 7/30/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import UIKit

//struct Colorkeys {
//    static let hueColor = "HueColor"
//}

@objc protocol JWOptionControlsViewControllerDelegate: class {
    func optionsControls(controller:JWOptionControlsViewController, colorForHueDidChange color:UIColor)
    func optionsControls(controller:JWOptionControlsViewController, colorForTrackDidChange color:UIColor)
    func optionsControls(controller:JWOptionControlsViewController, colorForTrackBottomDidChange color:UIColor)
}


class JWOptionControlsViewController: UIViewController {

    weak var delegate: JWOptionControlsViewControllerDelegate?
    
    @IBOutlet weak var controlsStackView: UIStackView!

    @IBOutlet weak var controlView1: UIView!
    @IBOutlet weak var controlsView1: UIStackView!

    @IBOutlet weak var controlView2: UIView!
    @IBOutlet weak var controlsView2: UIView!
    
    @IBOutlet weak var controlView1RevealButton: UIButton!
    @IBOutlet weak var controlView2RevealButton: UIButton!
    
    @IBOutlet weak var optionSlider1: UISlider!
    @IBOutlet weak var optionSlider2: UISlider!
    @IBOutlet weak var optionSlider3: UISlider!
    @IBOutlet weak var optionSlider4: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:
    
    @IBAction func stepper1ValueChanged(sender: UIStepper) {
        print("stepper1 \(sender.value)")
        
        if let color = color(with: Int(sender.value)) {
            delegate?.optionsControls(self, colorForHueDidChange:color)
        }
    }
    
    @IBAction func stepper2ValueChanged(sender: UIStepper) {
        print("stepper2 \(sender.value)")
        
        if let color = color(with: Int(sender.value)) {
            delegate?.optionsControls(self, colorForTrackDidChange:color)
        }
    }
    
    @IBAction func stepper3ValueChanged(sender: UIStepper) {
        print("stepper3 \(sender.value)")
        
        if let color = color(with: Int(sender.value)) {
            delegate?.optionsControls(self, colorForTrackBottomDidChange:color)
        }
    }
    
    
    func color(with colorIndex:Int) -> UIColor? {
        var selectedColor: UIColor?
        
        if (colorIndex == 0) {
            selectedColor = UIColor.whiteColor()
        } else if (colorIndex == 1) {
            selectedColor = UIColor.redColor()
        } else if (colorIndex == 2) {
            selectedColor = UIColor.greenColor()
        } else if (colorIndex == 3) {
            selectedColor = UIColor.blueColor()
        } else if (colorIndex == 4) {
            selectedColor = UIColor.blackColor()
        } else if (colorIndex == 5) {
            selectedColor = UIColor(red:128/255.0, green:128/255.0, blue:0/255.0, alpha:1.0) // asparagus
        }else if (colorIndex == 6) {
            selectedColor = UIColor(red:0/255.0, green:64/255.0, blue:128/255.0, alpha:1.0) // ocean
        }else if (colorIndex == 7) {
            selectedColor = UIColor(red:0/255.0, green:128/255.0, blue:255/255.0, alpha:1.0) // aqua
        }else if (colorIndex == 8) {
            
            //selectedColor = [UIColor colorWithRed:102/255.0 green:204/255.0 blue:255/255.0 alpha:1.0]; // sky
        }
        
        return selectedColor
    }

    

    @IBAction func buttonPressed(sender: UIButton) {
     
        if sender == controlView1RevealButton {
            print("button1")
            
            let newlyHidden = !controlsView1.hidden
            UIView.animateWithDuration(0.25, delay: 0, options: .CurveEaseOut, animations: {
                self.controlsView1.hidden = newlyHidden
                }, completion: { (fini) in
                    if newlyHidden {
                        sender.setTitle("Reveal", forState: .Normal)
                    } else {
                        sender.setTitle("Hide", forState: .Normal)
                    }
            })

            
        } else if sender == controlView2RevealButton {
            print("button2")
            
            let newlyHidden = !controlsView2.hidden
            UIView.animateWithDuration(0.25, delay: 0, options: .CurveEaseOut, animations: {
                self.controlsView2.hidden = newlyHidden
                }, completion: { (fini) in
                    if newlyHidden {
                        sender.setTitle("Reveal", forState: .Normal)
                    } else {
                        sender.setTitle("Hide", forState: .Normal)
                    }
            })

        }
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}




//            UIView.animateWithDuration(0.5, animations: {
//                self.controlsView1.hidden = newlyHidden
//                }
//            )

//            UIView.animateWithDuration(0.25, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.2, options: [], animations: {
//                self.controlsView1.hidden = newlyHidden
//
//                }, completion: { (fini) in
//                    if newlyHidden {
//                        sender.setTitle("Reveal", forState: .Normal)
//                    } else {
//                        sender.setTitle("Hide", forState: .Normal)
//                    }
//
//
//            })

//    UIView.animateWithDuration(0.5, animations: {
//    [unowned self] in
//    self.controlsView1.hidden = hidden
//    }
//    )




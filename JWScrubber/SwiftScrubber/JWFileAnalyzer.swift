//
//  JWFileAnalyzer.swift
//  JWScrubber
//
//  Created by JOSEPH KERR on 8/2/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import Foundation
import AVFoundation


@objc protocol JWFileAnalyzerDelegate: class {

    func fileAnalyzer(analyzer:JWFileAnalyzer, bufferReceived buffer:AVAudioPCMBuffer, atReadPosition readPosition:AVAudioFramePosition, loudestSample:Float)
    
    func fileAnalyzer(analyzer:JWFileAnalyzer, bufferReceivedEditing buffer:AVAudioPCMBuffer, atReadPosition readPosition:AVAudioFramePosition, loudestSample:Float)

    func fileAnalyzer(analyzer:JWFileAnalyzer, bufferReceived buffer:AVAudioPCMBuffer, atReadPosition readPosition:AVAudioFramePosition)

    

}


//    func fileAnalyzer(analyzer:JWFileAnalyzer, bufferReceivedWith trackId:String, buffer:AVAudioPCMBuffer, atReadPosition readPosition:AVAudioFramePosition, loudestSample:Float)
//
//    func fileAnalyzer(analyzer:JWFileAnalyzer, bufferReceivedEditingWith trackId:String, buffer:AVAudioPCMBuffer, atReadPosition readPosition:AVAudioFramePosition, loudestSample:Float)


//    func bufferReceived(with trackId:String, buffer:AVAudioPCMBuffer, atReadPosition readPosition:AVAudioFramePosition, loudestSample:Float)
//    func bufferReceivedEditing(with trackId:String, buffer:AVAudioPCMBuffer, atReadPosition readPosition:AVAudioFramePosition, loudestSample:Float)


@objc class JWFileAnalyzer: NSObject {
    
    var buffersSentCount: UInt = 0
    
    var fileUrl: NSURL?
    var trackId: String?
    
    var traceAnalyze = true
    
    var lastReadPosition: AVAudioFramePosition?
    
    weak var delegate: JWFileAnalyzerDelegate?
    

    func audioFileAnalyzerForFile(usingFileReference fileReference:JWPlayerFileInfo, edit:Bool){
        
        guard let audioFileURL = fileUrl,
            trackIdentifier = trackId
            else {
                return
        }
        
        audioFileAnalyzerForFile(audioFileURL, forTrackId: trackIdentifier, usingFileReference: fileReference, edit: edit, readsEntireAudioForLoudest: true)
    }

    
    func audioFileAnalyzerForFile(fileURL:NSURL, forTrackId trackId:String, usingFileReference fileReference:JWPlayerFileInfo, edit:Bool){
        audioFileAnalyzerForFile(fileURL, forTrackId: trackId, usingFileReference: fileReference, edit: edit, readsEntireAudioForLoudest: true)
    }
    
    
    func audioFileAnalyzerForFile(fileURL:NSURL, forTrackId trackId:String, usingFileReference fileReference:JWPlayerFileInfo?, edit:Bool, readsEntireAudioForLoudest: Bool ) {
    
        var audioFile:AVAudioFile
        
        do{
            audioFile = try AVAudioFile(forReading: fileURL)
        } catch {
            print(error)
            return
        }

        let processingFormat = audioFile.processingFormat
        let fileLength = audioFile.length
        
        var framesToReadCount: AVAudioFrameCount = 0
        var remainingFrameCount: AVAudioFrameCount = 0
        var startReadPosition: AVAudioFramePosition = 0
        
        if (readsEntireAudioForLoudest) {
            startReadPosition = 0;
            remainingFrameCount =  AVAudioFrameCount(fileLength - startReadPosition)
            
        } else {

            //NO Impl yet
        }
        
        framesToReadCount = remainingFrameCount

//    NSLog(@"%s startReadPosition %lld framesToReadCount %u",__func__,startReadPosition,framesToReadCount);

        let kBufferMaxFrameCapacity: AVAudioFrameCount = 18 * 1024
        
        var readBuffer: AVAudioPCMBuffer
        
        // Iterate through entire file DETERMINE loudest sample
        var  loudestSample:Float = 0.000000
        var  loudestSamplePosition:AVAudioFramePosition = 0

        audioFile.framePosition = startReadPosition

        var  frameCount:AVAudioFrameCount = 0 // frames read

        
        //While the number of frames read is less than the number of frames that need to be read
        
        while frameCount < framesToReadCount {
    
            let framesRemaining:AVAudioFrameCount  = framesToReadCount - frameCount
            
            // More frames to read ?
            if framesRemaining > 0 {
                
            } else {
                //NSLog(@"%s NO MORE framesRemaining: %u, %.3f secs. ",__func__,framesRemaining, framesRemaining / processingFormat.sampleRate);
                break;
            }
            
            let framesToRead:AVAudioFrameCount = (framesRemaining < kBufferMaxFrameCapacity) ? framesRemaining : kBufferMaxFrameCapacity;
            
            readBuffer = AVAudioPCMBuffer(PCMFormat: audioFile.processingFormat, frameCapacity:framesToRead)

            if traceAnalyze {
                print("framesRemaining: \(framesRemaining) secs \(Double(framesRemaining) / processingFormat.sampleRate)")
                print("framesToRead: \(framesToRead) secs \(Double(framesToRead) / processingFormat.sampleRate)")
            }

            do {
                try audioFile.readIntoBuffer(readBuffer)

                if traceAnalyze {
                    print("frameLength: \(readBuffer.frameLength) secs \(Float(Double(readBuffer.frameLength) / processingFormat.sampleRate))")
                }
                
            } catch {
                print(error)
                print("failed to read audio file: \(error)");
                //return NO;
                break
            }


            frameCount += readBuffer.frameLength
            
            for channelIndex in 0..<readBuffer.format.channelCount {
                
                let channelsIndex: Int = Int(channelIndex)
                
                let channels = UnsafeBufferPointer(start: readBuffer.floatChannelData, count: Int(readBuffer.format.channelCount))
                let floats = UnsafeBufferPointer(start: channels[channelsIndex], count: Int(readBuffer.frameLength))
                
                for frameIndex in 0..<readBuffer.frameLength {
                    let sampleLevel = floats[Int(frameIndex)]
                    let sampleAbsLevel = fabs(sampleLevel)
                    if sampleAbsLevel > loudestSample {
                        loudestSample = sampleAbsLevel
                        loudestSamplePosition = AVAudioFramePosition(frameIndex)
                    }
                }
            }
            
            if traceAnalyze {
                print("frameCount: \(frameCount) secs \(Double(frameCount) / processingFormat.sampleRate)")
            }

        }

        
        // ===============================================

        
        
        if let fileRef = fileReference where edit == false {
            
            // NOT Editing with a file reference
            
            fileRef.calculateAtCurrentPosition(fileRef.trackStartPosition)
            // TODO: add method that calculates from trackStartPosition

            if fileRef.readPositionInReferencedTrack < 0.0 {
                //NSLog(@"%s read position negative",__func__);
            } else {
                startReadPosition = AVAudioFramePosition(fileRef.readPositionInReferencedTrack *  Float(processingFormat.sampleRate))
                
                if traceAnalyze {
                    print("fileReference.dur \(fileRef.duration) fileReference.remainingInTrack\(fileRef.remainingInTrack) startReadPosition \(startReadPosition)")
                }
            }

            remainingFrameCount =  AVAudioFrameCount(fileRef.remainingInTrack * Float(processingFormat.sampleRate))
            
        } else {
            startReadPosition = 0;
            remainingFrameCount =  AVAudioFrameCount(fileLength - startReadPosition);
        }

        if traceAnalyze {
            print("generate buffers")
        }

        // Iterate again to generate buffer views

        framesToReadCount = remainingFrameCount
        frameCount = 0
        audioFile.framePosition = startReadPosition
        
        //NSUInteger track = [self trackNumberForTrackId:tid];
        ////    NSLog(@"%s startReadPosition %lld framesToReadCount %u",__func__,startReadPosition,framesToReadCount);
        //_buffersSentCounts[track]++;  // always have one more until we are finished
        
        buffersSentCount += 1
        
        let kBufferMaxFrame: AVAudioFrameCount = 16 * 1024 // 18
        
        while frameCount < framesToReadCount {
            
            let framesRemaining:AVAudioFrameCount  = framesToReadCount - frameCount
            //More frames to read
            if framesRemaining > 0 {

    
            } else {
                //NSLog(@"%s NO MORE framesRemaining: %u, %.3f secs. ",__func__,framesRemaining, framesRemaining / processingFormat.sampleRate);
                break;
            }
            
            let framesToRead:AVAudioFrameCount = (framesRemaining < kBufferMaxFrame) ? framesRemaining : kBufferMaxFrame;
            
            readBuffer = AVAudioPCMBuffer(PCMFormat: audioFile.processingFormat, frameCapacity:framesToRead)
            
            do {
                try audioFile.readIntoBuffer(readBuffer)
                
                if traceAnalyze {
                    print("framesRemaining: \(framesRemaining) secs \(Double(framesRemaining) / processingFormat.sampleRate)")
                    //print("framesToRead: \(framesToRead) secs \(Double(framesToRead) / processingFormat.sampleRate)")
                    //print("frameLength: \(readBuffer.frameLength) secs \(Float(Double(readBuffer.frameLength) / processingFormat.sampleRate))")
                }

            } catch {
                print(error)
                print("failed to read audio file: \(error)");
                //return NO;
                break
            }
            
            if readBuffer.frameLength > 0 {
                
                frameCount += readBuffer.frameLength
                
                let readPosition: AVAudioFramePosition = audioFile.framePosition
                
                if edit == false {
                    buffersSentCount += 1
                    delegate?.fileAnalyzer(self, bufferReceived: readBuffer, atReadPosition: readPosition, loudestSample: loudestSample)
                } else {
                    buffersSentCount += 1
                    delegate?.fileAnalyzer(self, bufferReceivedEditing:readBuffer, atReadPosition: readPosition, loudestSample: loudestSample)
                }
                
            } else {
                
            }
        
        }
        
        buffersSentCount -= 1

    }
    
    
    
    
    
    // opens file and reads from _recordingLastReadPosition
    
    func audioFileAnalyzerForRecorderFile() {
        
        guard let audioFileURL = fileUrl, trackIdentifier = trackId
            else {
                return
        }
        
        audioFileAnalyzerForRecorderFile(audioFileURL, forTrack: trackIdentifier)
    }
    
    
    func audioFileAnalyzerForRecorderFile(fileURL:NSURL, forTrack trackId:String) {
    
        // RESUMES from last read postion
        var audioFile: AVAudioFile
        
        do {
            audioFile = try AVAudioFile(forReading: fileURL)
        } catch {
            print("\(error) \(fileURL.lastPathComponent!)")
            return
        }
        
        if let lastPosition = lastReadPosition {
            audioFile.framePosition = lastPosition
        }
        
        audioFileAnalyzerForRecorderAudioFile(audioFile, forTrack:trackId)
        
        lastReadPosition = audioFile.framePosition
    }
    
    
//    // prototype - reading a file opened Once
//    -(void)a_udioFileAnalyzerForRecorderFile:(NSURL*)fileURL forTrackId:(NSString*)tid {
//    
//    NSError *error;
//    AVAudioFramePosition startReadPosition = 0;
//    
//    AVAudioFile *audioFile = [[AVAudioFile alloc] initForReading:fileURL error:&error];
//    if (audioFile) {
//    NSLog(@"%sFile: %@  ",__func__, [fileURL lastPathComponent]);
//    audioFile.framePosition = startReadPosition;
//    self.recordingAudioFile = audioFile;
//    [self audioFileAnalyzerForRecorderAudioFile:_recordingAudioFile forTrackId:tid];
//    } else {
//    NSLog(@"%sERROR File: %@  ",__func__, [fileURL lastPathComponent]);
//    }
//    }
    
    
    func audioFileAnalyzerForRecorderAudioFile(audioFile:AVAudioFile, forTrack trackId:String) {
    
        let processingFormat = audioFile.processingFormat
        let fileLength = audioFile.length
        let remainingFrameCount: AVAudioFrameCount = AVAudioFrameCount(fileLength - audioFile.framePosition )
        let framesToReadCount: AVAudioFrameCount = remainingFrameCount
        let kBufferMaxFrameCapacity: AVAudioFrameCount = 9 * 1024
        
        var frameCount: AVAudioFrameCount = 0

        while frameCount < framesToReadCount {
            
            let framesRemaining: AVAudioFrameCount = framesToReadCount - frameCount
            
            if framesRemaining > 0 {
                
                if framesRemaining < AVAudioFrameCount(Double(kBufferMaxFrameCapacity) * 0.5) {
                    // Done For Now
                    break;
                } else {
                    
                    //readBuffer = [[AVAudioPCMBuffer alloc] initWithPCMFormat:processingFormat frameCapacity:kBufferMaxFrameCapacity];
                    let framesToRead:AVAudioFrameCount = (framesRemaining < kBufferMaxFrameCapacity) ? framesRemaining : kBufferMaxFrameCapacity;
                    
                    let readBuffer = AVAudioPCMBuffer(PCMFormat: audioFile.processingFormat, frameCapacity:framesToRead)
                    
                    do {
                        try audioFile.readIntoBuffer(readBuffer)
                        
                        frameCount += readBuffer.frameLength

                        if traceAnalyze {
                            print("framesRemaining: \(framesRemaining) secs \(Double(framesRemaining) / processingFormat.sampleRate)")
                            //print("framesToRead: \(framesToRead) secs \(Double(framesToRead) / processingFormat.sampleRate)")
                            //print("frameLength: \(readBuffer.frameLength) secs \(Float(Double(readBuffer.frameLength) / processingFormat.sampleRate))")
                        }
                        
                        delegate?.fileAnalyzer(self, bufferReceived: readBuffer, atReadPosition: audioFile.framePosition)
                        
                    } catch {
                        print(error)
                        print("failed to read audio file: \(error)");
                        //return NO;
                        break
                    }
                }
                
            } //framesRemaining
            
        }
        
    }

    
//    //const AVAudioFrameCount kBufferMaxFrameCapacity = 9 * 1024L;
//    
//    AVAudioPCMBuffer *readBuffer = nil;
//    AVAudioFrameCount frameCount = 0; // frames read
//    NSUInteger iterations = 0;
//    
//    while (frameCount < framesToReadCount) {
//    
//    AVAudioFrameCount framesRemaining = framesToReadCount - frameCount;
//    
//    if (framesRemaining > 0) {
//    
//    if (framesRemaining < kBufferMaxFrameCapacity * 0.5 ) {
//    // Done For Now
//    break;
//    } else {
//    readBuffer = [[AVAudioPCMBuffer alloc] initWithPCMFormat:processingFormat frameCapacity:kBufferMaxFrameCapacity];
//    
//    NSError *error;
//    if ([audioFile readIntoBuffer: readBuffer error: &error]) {
//    frameCount += readBuffer.frameLength;
//    
//    [self bufferReceivedForTrackId:tid buffer:readBuffer atReadPosition:audioFile.framePosition];
//    }
//    }
//    
//    } else {
//    NSLog(@"%s NO MORE framesRemaining: %u, %.3f secs. ",__func__,framesRemaining, framesRemaining / processingFormat.sampleRate);
//    break;
//    }
//    
//    iterations++;
//    
//    }
    
    //    NSLog(@"iter %ld",iterations);
    
}




//                    [self bufferReceivedForEditingTrackId:tid buffer:readBuffer atReadPosition:readPosition loudestSample:loudestSample];
//                    _buffersSentCounts[0]++;
//                    _buffersSentCounts[track]++;










//while (frameCount < framesToReadCount) {
//
//    AVAudioFrameCount framesRemaining = framesToReadCount - frameCount;
//    if (framesRemaining > 0) {
//        // proceed with
//        AVAudioFrameCount framesToRead = (framesRemaining < kBufferMaxFrame) ? framesRemaining : kBufferMaxFrame;
//        readBuffer = [[AVAudioPCMBuffer alloc] initWithPCMFormat:audioFile.processingFormat frameCapacity:framesToRead];
//        #ifdef TRACEANALYZE
//        NSLog(@"%s framesRemaining: %u, %.3f secs. framesToRead %u, %.3f secs. Max %u",__func__,
//        framesRemaining, framesRemaining / processingFormat.sampleRate,
//        framesToRead, framesToRead / processingFormat.sampleRate,kBufferMaxFrame);
//#endif
//
//error = nil;
//if ([audioFile readIntoBuffer: readBuffer error: &error]) {
//
//    #ifdef TRACEANALYZE
//    NSLog(@"%s frameLength: %u, %.3f secs.",__func__,
//    readBuffer.frameLength, readBuffer.frameLength / processingFormat.sampleRate);
//#endif
//if (readBuffer.frameLength > 0) {
//
//    frameCount += readBuffer.frameLength;
//
//    AVAudioFramePosition readPosition = audioFile.framePosition;
//    if (editing == NO) {
//        [self bufferReceivedForTrackId:tid buffer:readBuffer atReadPosition:readPosition loudestSample:loudestSample];
//        _buffersSentCounts[track]++;
//    } else {
//        [self bufferReceivedForEditingTrackId:tid buffer:readBuffer atReadPosition:readPosition loudestSample:loudestSample];
//        _buffersSentCounts[0]++;
//        _buffersSentCounts[track]++;
//
//    }
//}
//
//} else {
//    NSLog(@"failed to read audio file: %@", error);
//    break;
//}
//
//#ifdef TRACEANALYZE
//NSLog(@"%s frameCount: %u, %.3f secs. framesToReadCount %u",__func__,frameCount, frameCount / processingFormat.sampleRate,framesToReadCount);
//#endif
//} else {
//    NSLog(@"%s NO MORE framesRemaining: %u, %.3f secs. ",__func__,framesRemaining, framesRemaining / processingFormat.sampleRate);
//    break;
//}
//}




//if edit == false && fileReference != nil {
//    fileReference!.calculateAtCurrentPosition(fileReference.trackStartPosition)
//
//    if (fileReference.readPositionInReferencedTrack < 0.0) {
//        NSLog(@"%s read position negative",__func__);
//    } else {
//        startReadPosition = fileReference.readPositionInReferencedTrack *  processingFormat.sampleRate;
//
//        #ifdef TRACEANALYZE
//        NSLog(@"%s ref dur %.2fs remaining %.2fs readpos %lld ",__func__,
//        fileReference.duration,
//        fileReference.remainingInTrack,
//        startReadPosition);
//#endif
//}
//
//remainingFrameCount =  fileReference.remainingInTrack * processingFormat.sampleRate;
//
//} else {
//    startReadPosition = 0;
//    remainingFrameCount =  (AVAudioFrameCount)(fileLength - startReadPosition);
//}




//for (AVAudioChannelCount channelIndex = 0; channelIndex < readBuffer.format.channelCount; ++channelIndex){
//    float *channelData = readBuffer.floatChannelData[channelIndex];
//    for (AVAudioFrameCount frameIndex = 0; frameIndex < readBuffer.frameLength; ++frameIndex){
//        float sampleAbsLevel = fabs(channelData[frameIndex]);
//        if (sampleAbsLevel > loudestSample){
//            loudestSample = sampleAbsLevel;
//            loudestSamplePosition = audioFile.framePosition + frameIndex;
//        }
//    }
//}

//} else {
//    NSLog(@"failed to read audio file: %@", error);
//    //return NO;
//    break;
//}





//        #ifdef TRACEANALYZE
//        NSLog(@"%sLength: %lld, %.3f seconds  \nFile: %@  ",__func__,
//        (long long)fileLength, fileLength / audioFile.fileFormat.sampleRate, [fileURL lastPathComponent]);
//    #endif


//NSError *error;
//    AVAudioFile *audioFile = [[AVAudioFile alloc] initForReading:fileURL error:&error];
//    AVAudioFormat *processingFormat = [audioFile processingFormat];
//    AVAudioFramePosition fileLength = audioFile.length;

//    #ifdef TRACEANALYZE
//    NSLog(@"%sLength: %lld, %.3f seconds  \nFile: %@  ",__func__,
//    (long long)fileLength, fileLength / audioFile.fileFormat.sampleRate, [fileURL lastPathComponent]);
//    #endif
//    AVAudioFrameCount framesToReadCount = 0;
//    // DETERMINE READPOS and TOTAL READLENGTH
//    // COMPUTE framesToReadCount
//    // NO FILEREF implies start at ZERO
//    // READ from the File at startReadPos for framesToReadCount
//    // COMPUTE framesToReadCount
//    AVAudioFrameCount remainingFrameCount = 0;
//    AVAudioFramePosition startReadPosition = 0;


// readsEntireAudioForLoudest
////    if (readsEntireAudioForLoudest) {
////    startReadPosition = 0;
////    remainingFrameCount =  (AVAudioFrameCount)(fileLength - startReadPosition);
////
////    } else {
//
//    if (editing == NO && fileReference) {
//    [fileReference calculateAtCurrentPosition:fileReference.trackStartPosition];
//
//    if (fileReference.readPositionInReferencedTrack < 0.0) {
//    NSLog(@"%s read position negative",__func__);
//    } else {
//    //Sample Rate: The number of times an analog signal is measured (sampled) per second
//    startReadPosition = fileReference.readPositionInReferencedTrack *  processingFormat.sampleRate;
//
//    #ifdef TRACEANALYZE
//    NSLog(@"%s ref dur %.2fs remaining %.2fs readpos %lld ",__func__,
//    fileReference.duration,
//    fileReference.remainingInTrack,
//    startReadPosition);
//    #endif
//}
//
//remainingFrameCount =  fileReference.remainingInTrack * processingFormat.sampleRate;
//
//} else {
//
//    startReadPosition = 0;
//    remainingFrameCount =  (AVAudioFrameCount)(fileLength - startReadPosition);
//}
//}


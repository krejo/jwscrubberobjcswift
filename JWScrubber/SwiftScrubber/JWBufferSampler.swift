//
//  JWBufferSampler.swift
//  JWScrubber
//
//  Created by JOSEPH KERR on 7/22/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import Foundation
import AVFoundation;

// SamplerSampleSize
@objc enum SamplerSize: Int {
    
    case SamplerSizeMin = 1,
    SamplerSize4,
    SamplerSize6,
    SamplerSize8,
    SamplerSize10,
    SamplerSize14,
    SamplerSize18,
    SamplerSize24,
    SamplerSizeMax
}

// This object uses Array Slices
//        let array1: Array<Int> = [1, 2, 3, 4, 5, 6]
//        let slice: ArraySlice<Int> = array1[0...2]
//
//        let array2: Array<Int> = [8, 9, 10]
//        let combined: Array<Int> = array2 + slice // [8, 9, 10, 1, 2, 3]
//
//        0.stride(to: 10, by: 2)
//        and
//        Array(0.stride(to: 10, by: 2)) // is [0, 2, 4, 6, 8]
//



class SwiftJWBufferSampler: NSObject {
    
    var samplerSize: SamplerSize
    var trackNumber: Int = 0
    
    private var loudestSampleSoFar: Float?
    private var loudestSampleAllBuffers: Float = 0.0

    var dualChannel: Bool = false
    var computeAverages: Bool = false
    var gatherPulseSamples: Bool = false
    
    // Readonly
    private(set) internal var samplesChannel1: [Float]?
    private(set) internal var samplesChannel2: [Float]?
    private(set) internal var averageChannel1: [Float]?
    private(set) internal var averageChannel2: [Float]?
    private(set) internal var durationThisBuffer: Double = 0.0
    private(set) internal var loudestSample: Float = 0.0
    
    // Pulse values
    private(set) internal var loudestSampleValue: Float = 0.0
    private(set) internal var lowestSampleValue: Float = 0.0
    private(set) internal var loudestSampleFramePostion: AVAudioFramePosition = 0
    private(set) internal var lowestSampleFramePostion: AVAudioFramePosition = 0
    
    private var completionMethod: (() -> Void)?

    
    override init() {
        samplerSize = .SamplerSize4
        super.init()
    }
    
    convenience init(sampleSize: SamplerSize) {
        self.init()
        samplerSize = sampleSize
    }
    
    // record
    
    convenience init(buffer: AVAudioPCMBuffer, readPosition:AVAudioFramePosition,
         sampleSize: SamplerSize, dualChannel: Bool, computeAverages: Bool,
         pulseSamples: Bool, loudestSampleSoFar: Float
         ) {
        
        self.init(sampleSize: sampleSize)
        self.dualChannel = dualChannel
        self.computeAverages = computeAverages
        self.gatherPulseSamples = pulseSamples
        sampleTheBuffer(buffer, atReadPosition: readPosition, loudestSampleSoFar: loudestSampleSoFar, completion: nil)
    }
    
    // play - we know loudest
    
    convenience init(buffer: AVAudioPCMBuffer, readPosition:AVAudioFramePosition,
         sampleSize: SamplerSize, dualChannel: Bool, computeAverages: Bool,
         pulseSamples: Bool, loudestSample: Float
        ) {
        
        self.init(sampleSize: sampleSize)
        self.dualChannel = dualChannel
        self.computeAverages = computeAverages
        self.gatherPulseSamples = pulseSamples
        sampleTheBuffer(buffer, atReadPosition: readPosition, loudestSampleAllBuffers: loudestSample, completion: nil)
    }
    
    
    // Record
  
    func sampleTheBuffer(buffer:AVAudioPCMBuffer, atReadPosition readPosition:AVAudioFramePosition, loudestSampleSoFar loudestSample:Float, completion: (() -> Void)? ) {

        self.loudestSampleSoFar = loudestSample
        sampleTheBuffer(buffer, atReadPosition: readPosition, loudestSample: loudestSample, completion: completion)
    }

    // Play
    
    func sampleTheBuffer(buffer:AVAudioPCMBuffer, atReadPosition readPosition:AVAudioFramePosition, loudestSampleAllBuffers  loudestSample:Float, completion: (() -> Void)? ) {
        
        self.loudestSampleAllBuffers = loudestSample
        sampleTheBuffer(buffer, atReadPosition: readPosition, loudestSample: loudestSample, completion: completion)
    }

    private var loudestSampleForPulse: Float = 0.0
    private var loudestSampleForPulseIndex = 0
    private var lowestSampleForPulse: Float = 0.0
    private var lowestSampleForPulseIndex = 0
    
    // Play / Record Common
    
    private func sampleTheBuffer(buffer:AVAudioPCMBuffer, atReadPosition readPosition:AVAudioFramePosition, loudestSample:Float, completion: (() -> Void)? ) {
        
        //UnsafePointer streamDescription
        let streamDescription = buffer.format.streamDescription.memory
        let mSampleRate = streamDescription.mSampleRate
        let duration = (1.0 / mSampleRate) * Double(streamDescription.mFramesPerPacket)
        
        durationThisBuffer = duration * Double(buffer.frameLength);
        lowestSampleForPulse = loudestSample
        completionMethod = completion
        
        bufferReceived(buffer, atReadPosition: readPosition, loudestSample: loudestSample, completion: completion)
    }

    
    private var samplingSize = 4
    private var samplesProcessedCh1 = 0
    private var samplesProcessedCh2 = 0
    private var loudestSamplesChannel1 = [Float](count: 32, repeatedValue: 0.0)
    private var loudestSamplesChannel2 = [Float](count: 32, repeatedValue: 0.0)
    private var averageSamplesChannel1 = [Float](count: 32, repeatedValue: 0.0)
    private var averageSamplesChannel2 = [Float](count: 32, repeatedValue: 0.0)
    
    // PLAY
    
    func bufferReceived(buffer:AVAudioPCMBuffer, atReadPosition readPosition:AVAudioFramePosition, loudestSample:Float, completion: (() -> Void)? ) {
        
        if buffer.frameLength == 0 {
            print("Empty buffer")
            return
        }
        
        let averages = computeAverages
        
        switch samplerSize {
        case .SamplerSize4:
            samplingSize = 4
        case .SamplerSize8:
            samplingSize = 8
        case .SamplerSize10:
            samplingSize = 10
        case .SamplerSize14:
            samplingSize = 14
        case .SamplerSize18:
            samplingSize = 18
        default:
            samplingSize = 4
        }

        
        for channelIndex in 0..<buffer.format.channelCount {

            let channelsIndex: Int = Int(channelIndex)

            if !dualChannel && channelsIndex > 0 {
                break;
            }
            
            let channels = UnsafeBufferPointer(start: buffer.floatChannelData, count: Int(buffer.format.channelCount))
            let floats = UnsafeBufferPointer(start: channels[channelsIndex], count: Int(buffer.frameLength))
            
            let framesPerSample = Int(buffer.frameLength) / samplingSize
            
            for sample in 0..<samplingSize {

                // For each sample create an Array slice and for that slice compute stats
                let startIndex = sample * framesPerSample
                let endIndex = startIndex + framesPerSample
                let arrSample = floats[startIndex..<endIndex]
                
                // Think Concurrent
                //dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0)){
                
                // From here compute the stats for this array Slice
                var loudestInSample: Float = 0.0
                var lowestInSample: Float = loudestSample
                var count = 0
                var sum: Float = 0.0
                var indexForTrackingFrame = 0

                // Cant Index into slice arrSample[i] use (for in)

                for sampleLevel in arrSample {
                    
                    // TODO: allow negative values?
                    let sampleAbsLevel = fabs(sampleLevel)
                    
                    if sampleAbsLevel > loudestInSample {
                        loudestInSample = sampleAbsLevel
                    }
                    
                    if sampleAbsLevel < lowestInSample {
                        lowestInSample = sampleAbsLevel
                    }
                    
                    if (averages) {
                        sum += sampleAbsLevel
                        count += 1
                    }
                    
                    indexForTrackingFrame += 1
                }
                
                let averageSample = sum / Float(count)
                
                self.collateResults(sample, loudest: loudestInSample, lowest: lowestInSample, average: averageSample, channel: channelsIndex+1)
                
            //} // dispatch
                
                
            }  // For each sample
        }
        
    }
    
    
    func collateResults(sample: Int, loudest: Float, lowest: Float, average:Float, channel:Int ) {
        
        //print("Sample \(sample)  loudestSample \(loudest) samplesProcessed \(samplesProcessed)")
        //print("collating \(sample) channel \(channel)")
        
        if channel == 1 {
            samplesProcessedCh1 += 1

            loudestSamplesChannel1[sample] = loudest
            if computeAverages {
                averageSamplesChannel1[sample] = average
            }
            
        } else if channel == 2 {
            samplesProcessedCh2 += 1

            loudestSamplesChannel2[sample] = loudest
            if computeAverages {
                averageSamplesChannel2[sample] = average
            }
        }
        
        
        if let loudestSoFar = loudestSampleSoFar {
            // Recording uses the API that sets loudest sample so far
            // where loudest all buffers is not initially known
        
            if loudest > loudestSoFar {
                loudestSampleSoFar = loudest
            }
            
            if loudest > loudestSample {
                loudestSample = loudest
            }
        }
        

        if gatherPulseSamples {
            if loudest > loudestSampleForPulse {
                loudestSampleForPulse = loudest;
                // loudestSampleForPulseIndex = index;
            }
            
            if lowest < lowestSampleForPulse {
                lowestSampleForPulse = lowest;
//                lowestSampleForPulseIndex = index;
            }
        }
        
        
        // Check for completion
        
        if samplesProcessedCh1 < samplingSize {
            //print("processing channel 1")
        } else if  dualChannel && samplesProcessedCh2 < samplingSize {
            //print("processing channel 2")
        } else {
            //print("Done collating")
            
            computeSamples()
            
            if let callCompletion = completionMethod {
                callCompletion()
            }
        }
    }
    
    
    func computeSamples() {
        
        if gatherPulseSamples {
            // Set pulse values for consumption
            loudestSampleValue = loudestSampleForPulse/loudestSampleAllBuffers;
            lowestSampleValue = lowestSampleForPulse/loudestSampleAllBuffers;
        }
        
//            self.loudestSampleFramePostion = loudestPulseSampleFramePostion;
//            self.lowestSampleFramePostion = lowestPulseSampleFramePostion;
        
        // This will be
        //  loudestSampleAllBuffers  if playing
        //  loudestSampleSoFar  if recording

        var loudestOfAll: Float = 0.0

        if let loudestSoFar = loudestSampleSoFar {
            // Recording uses the API that sets loudest sample so far
            // where loudest all buffers is not initially known
            loudestOfAll = loudestSoFar
        } else {
            // Playing
            loudestOfAll = loudestSampleAllBuffers
        }

        self.samplesChannel1 = loudestSamplesChannel1[0..<samplingSize].map{Float($0)/loudestOfAll}
        if dualChannel {
            self.samplesChannel2 = loudestSamplesChannel2[0..<samplingSize].map{Float($0)/loudestOfAll}
        }
        
        if computeAverages {
            self.averageChannel1 = averageSamplesChannel1[0..<samplingSize].map{Float($0)/loudestOfAll}
            if dualChannel {
                self.averageChannel2 = averageSamplesChannel2[0..<samplingSize].map{Float($0)/loudestOfAll}
            }
        }
    }

}


// http://stackoverflow.com/questions/24090119/from-unsafepointerunsafepointercfloat-to-an-array-of-floats-in-swift
//            let channels = UnsafeBufferPointer(start: myAudioBuffer.floatChannelData, count: Int(myAudioBuffer.format.channelCount))
//            let floats = UnsafeBufferPointer(start: channels[0], count: Int(myAudioBuffer.frameLength))

// AUDIO CHANNEL DATA

//        for (AVAudioChannelCount channelIndex = 0; channelIndex < buffer.format.channelCount; ++channelIndex)
//        {
//            if (_dualChannel == NO) {
//                if (channelIndex > 0) {
//                    break;
//                }
//            }
//            float *channelData = buffer.floatChannelData[channelIndex];
//            for (AVAudioFrameCount frameIndex = 0; frameIndex < buffer.frameLength; ++frameIndex)
//            {
//                float sampleAbsLevel = fabs(channelData[frameIndex]);
//                // CHANNEL 1
//                if (channelIndex == 0) {
//                }
//            }
//        }



//
//  JWBufferController.swift
//  JWScrubber
//
//  Created by JOSEPH KERR on 8/3/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import Foundation
import AVFoundation

@objc protocol JWBufferControllerDelegate: class {

    func bufferController(controller:JWBufferController, trackInfoFor trackId:String) -> [String : AnyObject]?
    func bufferControllerWillCollectPulseData(controller:JWBufferController)
    func bufferController(controller:JWBufferController, durationForTrack trackNumber:Int) -> Double
    func bufferController(controller:JWBufferController, durationForTrack trackNumber:Int, durationThisBuffer duration:Double)
    func bufferController(controller:JWBufferController, durationForTrack trackNumber:Int, durationThisRecordingBuffer duration:Double)

    func bufferController(controller:JWBufferController, bufferSampledForTrack trackId:String, trackNumber:Int,
                          bufferSampler:SwiftJWBufferSampler,
                          startDuration:Double,
                          duration:Double,
                          sequence: Int,
                          advance: Bool,
                          recording: Bool,
                          editing: Bool
                          )

    func bufferController(controller:JWBufferController, pulseDataCollectedForTrack trackId:String,
                          bufferSampler:SwiftJWBufferSampler, frameLength:AVAudioFrameCount)
    
}

//func bufferController(controller:JWBufferController, trackColorsFor trackId:String) -> [String : AnyObject]?
//func bufferController(controller:JWBufferController, buffersSentCountForTrack trackNumber:Int) -> Int


@objc class JWBufferController: NSObject {
    
    var bufferCount: UInt = 0

    weak var delegate: JWBufferControllerDelegate?
    
    private var  bufferReceivedQueue: dispatch_queue_t!
    private var  bufferSampledQueue: dispatch_queue_t!
    private var  bufferReceivedPerformanceQueue: dispatch_queue_t!
    private var  bufferSampledPerformanceQueue: dispatch_queue_t!

    private var buffersReceivedCounts: [UInt]!
    private var buffersReceivedCount: UInt = 0
    
    private var loudestSamplesInTracks: [Float]!
    private var elapsedTimesSoFar: [Float]!
    
    override init() {
        bufferReceivedQueue =
            dispatch_queue_create("bufferReceived",
                                  dispatch_queue_attr_make_with_qos_class(DISPATCH_QUEUE_SERIAL,QOS_CLASS_USER_INTERACTIVE, -1))

        bufferSampledQueue =
            dispatch_queue_create("bufferProcessing",
                                  dispatch_queue_attr_make_with_qos_class(DISPATCH_QUEUE_SERIAL,QOS_CLASS_USER_INTERACTIVE, -1))
        
        bufferReceivedPerformanceQueue =
            dispatch_queue_create("bufferReceivedP",
                                  dispatch_queue_attr_make_with_qos_class(DISPATCH_QUEUE_SERIAL,QOS_CLASS_USER_INTERACTIVE, -1))
        
        bufferSampledPerformanceQueue =
            dispatch_queue_create("bufferProcessingP",
                                  dispatch_queue_attr_make_with_qos_class(DISPATCH_QUEUE_SERIAL,QOS_CLASS_USER_INTERACTIVE, -1))

        buffersReceivedCounts = [UInt](count: 10, repeatedValue:0)
        loudestSamplesInTracks = [Float](count: 10, repeatedValue:0.0)
        elapsedTimesSoFar = [Float](count: 10, repeatedValue:0.0)
        super.init()
    }
    
    
    
    func elapsedTimeSoFarForTrack(track: Int) -> Float {
        return elapsedTimesSoFar[track]
        
    }
    
    
    struct BufferConfig {
        var track:Int = 0
        var sampleSz: SampleSize = .Size14
        var options: SamplingOptions = .None
        var dualChannel:Bool = false
        var computeAverages:Bool = false
        var collectPulseData: Bool = false
        var startDuration: Double = 0.0
        var durationThisBuffer: Double = 0.0
        var samplerSize: SamplerSize = .SamplerSize14
        
        init(){ }
    }
    
    private func bufferConfig(with buffer:AVAudioPCMBuffer, trackId:String) -> BufferConfig {
        
        var result = BufferConfig()
        
        // Identify the track
        
        let trackInfo = delegate?.bufferController(self, trackInfoFor: trackId)
        
        // GET the options and Configure them
        
        var track:Int = 0
        
        if let infoForTrack = trackInfo {
            track = infoForTrack["tracknum"] as! Int
            let trackOptions = trackInfoOptions(infoForTrack)
            result.sampleSz = trackOptions.sampleSize
            result.options = trackOptions.options
        }
        
        result.track = track
        
        let dc:Bool = result.options.contains(.DualChannel)  // dual channel
        let computeAverages:Bool = !result.options.contains(.NoAverages)
        let collectPulseData: Bool = result.options.contains(.CollectPulseData)
        
        result.dualChannel = dc
        result.computeAverages = computeAverages
        result.collectPulseData = collectPulseData
        
        let sampleDurations = sampleInfo(buffer, track:track)
        
        result.startDuration = sampleDurations.startDur
        result.durationThisBuffer = sampleDurations.durThisBuffer
        result.samplerSize = sampleSizeTranslation(result.sampleSz)
        
        return result
    }

    
    // MARK: - Buffer received
    
    // PLAY
    
    func bufferReceivedForTrackId(trackId:String, buffer:AVAudioPCMBuffer, atReadPosition readPosition:AVAudioFramePosition, loudestSample loudestSampleAllBuffers:Float) {
        
        let bc = bufferConfig(with: buffer, trackId: trackId )
        
        delegate?.bufferController(self, durationForTrack: bc.track, durationThisBuffer: bc.durationThisBuffer)
        
        buffersReceivedCounts[bc.track] += 1
        let bufferNumber = buffersReceivedCounts[bc.track]
        
        if bc.collectPulseData {
            delegate?.bufferControllerWillCollectPulseData(self)
        }

        dispatch_async(bufferReceivedQueue, {
            
            let sampler = SwiftJWBufferSampler()
            sampler.dualChannel = bc.dualChannel;
            sampler.gatherPulseSamples = bc.collectPulseData;
            sampler.computeAverages = bc.computeAverages;
            sampler.samplerSize = bc.samplerSize;
            
            // Synchrounos call SwiftJWBufferSampler
            
            sampler.sampleTheBuffer(buffer, atReadPosition: readPosition, loudestSampleAllBuffers: loudestSampleAllBuffers, completion: nil)
            
            // when finished sampling add the buffer to the view
            dispatch_async(self.bufferSampledQueue, {
                
                self.delegate?.bufferController(self, bufferSampledForTrack:trackId, trackNumber:bc.track,
                    bufferSampler:sampler,
                    startDuration: bc.startDuration,
                    duration: bc.durationThisBuffer,
                    sequence: Int(bufferNumber),
                    advance: false,
                    recording: false,
                    editing: false
                )
                
                if bc.collectPulseData {
                    self.delegate?.bufferController(self, pulseDataCollectedForTrack:trackId, bufferSampler:sampler, frameLength:buffer.frameLength)
                }
                
            }) //bufferSampledQueue
            
        }) //_bufferReceivedQueue
    }
    
    
    // PLAY EDIT
    
    func bufferReceivedForEditingTrackId(trackId:String, buffer:AVAudioPCMBuffer, atReadPosition readPosition:AVAudioFramePosition,
                                         loudestSample loudestSampleAllBuffers:Float) {
        
        let bc = bufferConfig(with: buffer, trackId: trackId )
        
        delegate?.bufferController(self, durationForTrack: bc.track, durationThisBuffer: bc.durationThisBuffer)
        
        buffersReceivedCounts[0] += 1
        let bufferNumber = buffersReceivedCounts[0]
        
        dispatch_async(bufferReceivedQueue, {
            
            let sampler = SwiftJWBufferSampler()
            sampler.dualChannel = bc.dualChannel;
            sampler.gatherPulseSamples = bc.collectPulseData;
            sampler.computeAverages = bc.computeAverages;
            sampler.samplerSize = bc.samplerSize;
            
            // Synchrounos call SwiftJWBufferSampler
            
            sampler.sampleTheBuffer(buffer, atReadPosition: readPosition, loudestSampleAllBuffers: loudestSampleAllBuffers, completion: nil)
            
            // when finished sampling add the buffer to the view
            dispatch_async(self.bufferSampledQueue, {
                
                self.delegate?.bufferController(self, bufferSampledForTrack:trackId, trackNumber:bc.track,
                    bufferSampler:sampler,
                    startDuration:bc.startDuration,
                    duration:bc.durationThisBuffer,
                    sequence: Int(bufferNumber),
                    advance: false,
                    recording: false,
                    editing: true
                )
            })
        })
       
    }
    
    
    // RECORD
    
    func bufferReceivedForTrackId(trackId:String, buffer:AVAudioPCMBuffer, atReadPosition readPosition:AVAudioFramePosition) {
        
        let bc = bufferConfig(with: buffer, trackId: trackId )
        
        delegate?.bufferController(self, durationForTrack: bc.track, durationThisBuffer: bc.durationThisBuffer)
        
        buffersReceivedCounts[bc.track] += 1
        let bufferNumber = buffersReceivedCounts[bc.track]

        buffersReceivedCount += 1
        
        //let loudestSampleSoFar = loudestSamplesInTracks[bc.track]
        
        if bc.collectPulseData {
            delegate?.bufferControllerWillCollectPulseData(self)
        }
        
        dispatch_async(bufferReceivedPerformanceQueue, {
            
            let sampler = SwiftJWBufferSampler()
            sampler.dualChannel = bc.dualChannel;
            sampler.gatherPulseSamples = bc.collectPulseData;
            sampler.computeAverages = bc.computeAverages;
            sampler.samplerSize = bc.samplerSize;
            
            if sampler.loudestSample > self.loudestSamplesInTracks[bc.track] {
                self.loudestSamplesInTracks[bc.track] = sampler.loudestSample;
            }
            
            self.elapsedTimesSoFar[bc.track] += Float(sampler.durationThisBuffer)
            
            // Synchrounos call SwiftJWBufferSampler
            
            sampler.sampleTheBuffer(buffer, atReadPosition: readPosition, loudestSampleSoFar: self.loudestSamplesInTracks[bc.track], completion:nil )
            
            // when finished sampling add the buffer to the view
            dispatch_async(self.bufferSampledPerformanceQueue, {
                
                self.delegate?.bufferController(self, bufferSampledForTrack:trackId, trackNumber:bc.track,
                    bufferSampler:sampler,
                    startDuration:bc.startDuration,
                    duration:bc.durationThisBuffer,
                    sequence: Int(bufferNumber),
                    advance: false,
                    recording: true,
                    editing: false
                )
                
                if bc.collectPulseData {
                    self.delegate?.bufferController(self, pulseDataCollectedForTrack:trackId, bufferSampler:sampler, frameLength:buffer.frameLength)
                }
            
            }) //bufferSampledQueue
            
        }) //_bufferReceivedQueue

    }
    
    
    // MARK: Helpers
    
    private func sampleInfo(buffer:AVAudioPCMBuffer, track:Int) -> (startDur: Double, durThisBuffer: Double) {
        
        let startDuration = delegate?.bufferController(self, durationForTrack: track)
        
        //UnsafePointer streamDescription
        let streamDescription = buffer.format.streamDescription.memory
        let mSampleRate = streamDescription.mSampleRate
        let duration = (1.0 / mSampleRate) * Double(streamDescription.mFramesPerPacket)
        
        let durationThisBuffer = duration * Double(buffer.frameLength);
        
        return (startDuration!,durationThisBuffer)
    }
    
    private func sampleSizeTranslation(sampleSize:SampleSize) -> SamplerSize {
        
        var samplerSize: SamplerSize = .SamplerSize4
        if sampleSize == .Size4 {
            samplerSize = .SamplerSize4
        } else if sampleSize == .Size6 {
            samplerSize = .SamplerSize6
        } else if sampleSize == .Size8 {
            samplerSize = .SamplerSize8
        } else if sampleSize == .Size10 {
            samplerSize = .SamplerSize10
        } else if sampleSize == .Size14 {
            samplerSize = .SamplerSize14
        } else if sampleSize == .Size18 {
            samplerSize = .SamplerSize18
        }
        return samplerSize
    }
    
    private func trackInfoOptions(trackInfo:[String:AnyObject]) -> (options: SamplingOptions, sampleSize: SampleSize) {
        
        var sampleSz: SampleSize
        
        if let configuredSampleSize = trackInfo["samplesize"] as? Int ,
            let ssz = SampleSize(rawValue: configuredSampleSize) {
            sampleSz = ssz
        } else {
            sampleSz = .Size14
        }
        
        var options: SamplingOptions
        if let configuredOptions = trackInfo["options"] as? Int {
            options = SamplingOptions(rawValue: configuredOptions)
        } else {
            options = [.DualChannel]
        }
        
        return (options, sampleSz)
    }
    
    
}




//        let typeOptions: VABKindOptions = .Center
//        let layoutOptions: VABLayoutOptions = [.ShowAverageSamples, .ShowCenterLine]
//let options: SamplingOptions = [.DualChannel, .NoAverages]
//        let options: SamplingOptions = [.DualChannel]



//        let configuredSampleSize: SampleSize = trackInfo["samplesize"] as
//
//    SampleSize sz = [_tracks[tid][@"samplesize"] unsignedIntValue];
//    SamplerSampleSize bufferSampleSize = SSampleSizeMin;
//    if (sz ==SampleSize4) {
//    bufferSampleSize = SSampleSize4;
//    } else if (sz ==SampleSize6) {
//    bufferSampleSize = SSampleSize6;
//    } else if (sz ==SampleSize8) {
//    bufferSampleSize = SSampleSize8;
//    } else if (sz ==SampleSize10) {
//    bufferSampleSize = SSampleSize10;
//    } else if (sz ==SampleSize14) {
//    bufferSampleSize = SSampleSize14;
//    } else if (sz ==SampleSize18) {
//    bufferSampleSize = SSampleSize18;
//    }







//    #else
//
//    [sampler sampleTheBuffer:buffer atReadPosition:readPosition loudestSampleAllBuffers:loudestSampleAllBuffers completion:^{
//    
//    // when finished sampling add the buffer to the view
//    dispatch_async(_bufferSampledQueue, ^{
//    
//    NSUInteger finalValue;
//    //            if (_buffersSentCounts[track] > _buffersReceivedCounts[track])
//    //                finalValue = 0; // not finsihed
//    //            else
//    finalValue = _buffersSentCounts[track];  // finished heres the number
//    
//    //        averageSamples:sampler.averageSamples
//    //        averageSamples2:sampler.averageSamplesChannel2
//    
//    [self.scrubber addAudioViewChannelSamples:sampler.samplesChannel1
//    averageSamples:sampler.averageChannel1
//    channel2Samples:sampler.samplesChannel2
//    averageSamples2:sampler.averageChannel2
//    inTrack:track
//    startDuration:(NSTimeInterval)startDuration
//    duration:(NSTimeInterval)sampler.durationThisBuffer
//    final:finalValue
//    options:options
//    type:typeOptions
//    layout:layoutOptions
//    colors:trackColors
//    bufferSeq:bufferNo
//    autoAdvance:NO
//    recording:NO
//    editing:NO
//    size:_scrubberControllerSize ];
//    
//    }); //_bufferSampledQueue
//    }];
//    }); //_bufferReceivedQueue
//    
//    //
//    //        SwiftJWBufferSampler *sampler2 =
//    //        [[SwiftJWBufferSampler alloc] initWithBuffer:buffer readPosition:readPosition
//    //                                          sampleSize:bufferSampleSize
//    //                                         dualChannel:dc
//    //                                     computeAverages:computeAverages
//    //                                        pulseSamples:collectPulseData
//    //                                       loudestSample:loudestSampleAllBuffers ];
//    //
//    //         ];
//    
//    #endif
//    
//    #else
//    dispatch_async(_bufferReceivedQueue, ^{
//    
//    JWBufferSampler *bufferSampler =
//    [[JWBufferSampler alloc] initWithBuffer:buffer atReadPosition:readPosition
//    sampleSize:bufferSampleSize
//    dualChannel:dc
//    computeAverages:computeAverages
//    pulseSamples:collectPulseData
//    loudestSample:loudestSampleAllBuffers ];
//    
//    if (collectPulseData) {
//    [self.pulseSamplesDurations addObject:@(bufferSampler.durationThisBuffer)];
//    CGFloat pulsePosLowest = (CGFloat)bufferSampler.lowestSampleFramePostion / buffer.frameLength;
//    CGFloat pulsePosLoudest = (CGFloat)bufferSampler.loudestSampleFramePostion / buffer.frameLength;
//    /*
//     Each element has four values
//     valuePos , value , valuePos , value
//     Describing two samples the lowest and highest
//     */
//    if (pulsePosLowest > pulsePosLoudest) {
//    [self.pulseSamples addObject:@[@(pulsePosLoudest),@(bufferSampler.loudestSampleValue),  @(pulsePosLowest),@(bufferSampler.lowestSampleValue)]];
//    } else {
//    [self.pulseSamples addObject:@[@(pulsePosLowest),@(bufferSampler.lowestSampleValue),  @(pulsePosLoudest),@(bufferSampler.loudestSampleValue)]];
//    }
//    }
//    
//    
//    // when finished sampling add the buffer to the view
//    dispatch_async(_bufferSampledQueue, ^{
//    
//    NSUInteger finalValue;
//    //            if (_buffersSentCounts[track] > _buffersReceivedCounts[track])
//    //                finalValue = 0; // not finsihed
//    //            else
//    finalValue = _buffersSentCounts[track];  // finished heres the number
//    
//    [self.scrubber addAudioViewChannelSamples:bufferSampler.samples
//    averageSamples:bufferSampler.averageSamples
//    channel2Samples:bufferSampler.samplesChannel2
//    averageSamples2:bufferSampler.averageSamplesChannel2
//    inTrack:track
//    startDuration:(NSTimeInterval)startDuration
//    duration:(NSTimeInterval)bufferSampler.durationThisBuffer
//    final:finalValue
//    options:options
//    type:typeOptions
//    layout:layoutOptions
//    colors:trackColors
//    bufferSeq:bufferNo
//    autoAdvance:NO
//    recording:NO
//    editing:NO
//    size:_scrubberControllerSize ];
//    }); //_bufferSampledQueue
//    
//    }); //_bufferReceivedQueue
//    
//    #endif

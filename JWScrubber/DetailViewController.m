//
//  DetailViewController.m
//  JWScrubber
//
//  Created by JOSEPH KERR on 11/17/15.
//  Copyright © 2015 JOSEPH KERR. All rights reserved.
//

#import "DetailViewController.h"
#import "JWScrubberViewController.h"
#import "JWScrubberController.h"
#import "JWScrubber.h"
#import "JWScrubberOptionsTableViewController.h"
#import "JWScrubberD-Swift.h"

@interface DetailViewController () <JWScrubberControllerDelegate,JWScrubberOptionsDelegate,
JWOptionControlsViewControllerDelegate
> {
    UIColor *iosColor2;
    UIColor *iosColor1;
    UIColor *iosColor3;
    UIColor *iosColor4;
    BOOL colorizedTracks;
}
@property (strong, nonatomic) IBOutlet UIBarButtonItem *rewindButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *pauseButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *playButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *saveButton;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintScrubberHeight;
@property (strong, nonatomic) IBOutlet UIView *scrubberContainer;
@property (strong, nonatomic) JWScrubberViewController *scrubber;
@property (strong, nonatomic) JWScrubberController *scrubberController;
@property (strong, nonatomic) NSMutableDictionary *scrubberOptions;
@property (strong, nonatomic) NSDictionary *scrubberColors;
@property (strong, nonatomic) NSDictionary *scrubberTrackColors;
@property (strong, nonatomic) NSString *scrubberTrackId;
@property (nonatomic) ScrubberViewOptions viewOptions;
@property (strong, nonatomic) NSDate *editChangeTimeStamp;
@property (strong, nonatomic) NSDate *editChangeUpdateTimeStamp;
@property (strong, nonatomic) NSDate *positionChangeUpdateTimeStamp;
@property (strong, nonatomic) id currentEditTrackInfo;
@property (nonatomic) CGFloat currentPositionChange;

@property (strong, nonatomic) JWSliderControlsViewController *sliderControls;
@property (strong, nonatomic) JWOptionControlsViewController *optionControls;
@end


@implementation DetailViewController

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem {
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        NSLog(@"%s %@",__func__,[_detailItem description]);
        
        [self updateFromDetailItem];
        
        // Update the view.
        [self configureView];
    }
}

- (void)configureView {
    // Update the user interface for the detail item.
    if (self.detailItem) {
//        self.detailDescriptionLabel.text = [self.detailItem description];
        
        self.detailDescriptionLabel.text = self.detailItem[@"title"];
//        [NSString stringWithFormat:@"%@%@",self.detailItem[@"title"],self.detailItem[@"key"]];

    }
    
    if (_scrubberController) {
        [self configureScrubbers];
    }
  
}

- (void)viewDidLoad {
    [super viewDidLoad];
    iosColor1 = [UIColor colorWithRed:128/255.0 green:128/255.0 blue:0/255.0 alpha:1.0]; // asparagus
    iosColor2 = [UIColor colorWithRed:0/255.0 green:64/255.0 blue:128/255.0 alpha:1.0]; // ocean
    iosColor3 = [UIColor colorWithRed:0/255.0 green:128/255.0 blue:255/255.0 alpha:1.0]; // aqua
    iosColor4 = [UIColor colorWithRed:102/255.0 green:204/255.0 blue:255/255.0 alpha:1.0]; // sky
    colorizedTracks = YES;
    _pauseButton.enabled = NO;
    
    // We are given this item and we hold a strong reference to it
    
    [self configureView];
    
    [self.navigationController setToolbarHidden:NO];
    
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (self.isMovingFromParentViewController) {
        NSLog(@"%s LEAVING",__func__);
        
        if (self.editing) {
            self.editing = NO;
            [_scrubberController stopEditingTrackCancel:_scrubberTrackId];
        }

    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    NSLog(@"%s",__func__);
    // SCRUBBER VIEW
    if ([segue.identifier isEqualToString:@"JWScrubberView"]) {
        self.scrubber = (JWScrubberViewController*)segue.destinationViewController;
        self.scrubberController = [[JWScrubberController alloc] initWithScrubber:self.scrubber];
        self.scrubberController.delegate = self;
        
    } else if ([segue.identifier isEqualToString:@"JWDetailToOptionsSegue"]) {
        
        UINavigationController *nc = segue.destinationViewController;
        UIViewController *vc = nc.viewControllers[0];
        
        [(JWScrubberOptionsTableViewController *)vc setOptionsData:_scrubberOptions];
        [(JWScrubberOptionsTableViewController *)vc setScrubberColors:_scrubberColors];
        [(JWScrubberOptionsTableViewController *)vc setScrubberTrackColors:_scrubberTrackColors];
        [(JWScrubberOptionsTableViewController *)vc setViewOptions:_viewOptions];
        
        [(JWScrubberOptionsTableViewController *)vc setDelegate:self];
        
    } else if ([segue.identifier isEqualToString:@"JWScrubberSliders"]) {

        // Save the reference so configure scrubbers can use it
        
        self.sliderControls =  (JWSliderControlsViewController*)segue.destinationViewController;
        
    } else if ([segue.identifier isEqualToString:@"JWOptionScrubberSliders"]) {
    
    // Save the reference so configure scrubbers can use it
    
        JWOptionControlsViewController *optionControls =  (JWOptionControlsViewController*)segue.destinationViewController;
        optionControls.delegate = self;
        
        self.optionControls = optionControls;
    }


}


/*
 INFO
 @"key":cacheKey,
 @"options":scrubberOptions,
 @"title":@"scrubber",
 @"tracks":@(1),
 @"starttime":@(0.0),
 @"referencefile": scrubberFileReference,
 @"date":[NSDate date]
 @"viewoptions":
 OPTIONS
 // @"size":@(SampleSize14),
 // @"kind":@(VABOptionCenter),
 // @"layout":@(VABLayoutOptionOverlayAverages | VABLayoutOptionShowAverageSamples | VABLayoutOptionShowCenterLine)
 REFERENCEFILE
 //@"duration":@(0),
 //@"startinset":@(0.0),
 //@"endinset":@(0.0),
 COLORS
 @"trackcolors":
 @"scrubbercolors":
 
*/

- (void)updateFromDetailItem{
    if (_detailItem ) {
        _scrubberOptions = _detailItem[@"options"];
        
        NSMutableDictionary * scrubberFileReference = _detailItem[@"referencefile"];
        float startInset = [scrubberFileReference[@"startinset"] floatValue];
        float endInset = [scrubberFileReference[@"endinset"] floatValue];
        float duration = [scrubberFileReference[@"duration"] floatValue];
        NSLog(@"%s %.2f %.2f %.2f",__func__,startInset,endInset,duration);
        
        NSUInteger tracks =  [_detailItem[@"tracks"] unsignedIntegerValue];
        NSLog(@"%s tracks %ld",__func__,tracks);
        
        id viewOptionsObj =_detailItem[@"viewoptions"];
        if (viewOptionsObj) {
            _viewOptions = [viewOptionsObj unsignedIntegerValue];
        } else {
            _viewOptions = ScrubberViewOptionDisplayFullView;
            _detailItem[@"viewoptions"] = @(_viewOptions);
        }
        _scrubberTrackColors =  _detailItem[@"trackcolors"];
        _scrubberColors =  _detailItem[@"scrubbercolors"];
    }
}


#pragma mark - delegate

-(void)willComplete:(id)controller {
 
    NSLog(@"%s",__func__);

    if ([controller isKindOfClass:[JWScrubberOptionsTableViewController class]]) {
        // Update all of thethings that could have changed
        _detailItem[@"viewoptions"] = @([(JWScrubberOptionsTableViewController*)controller viewOptions]);
        _detailItem[@"options"] = [(JWScrubberOptionsTableViewController*)controller optionsData];
        _detailItem[@"trackcolors"] = [(JWScrubberOptionsTableViewController*)controller scrubberTrackColors];
        _detailItem[@"scrubbercolors"] =[(JWScrubberOptionsTableViewController*)controller scrubberColors];
        
        [self updateFromDetailItem];
        [self configureScrubbers];
        
        [_delegate itemChanged:self cachKey:_detailItem[@"key"]];
    }
}


-(void)selectionMade:(JWScrubberOptionsTableViewController*)controller {
    
    NSLog(@"%s",__func__);
    
    _detailItem[@"trackcolors"] = [(JWScrubberOptionsTableViewController*)controller scrubberTrackColors];
    _detailItem[@"scrubbercolors"] =[(JWScrubberOptionsTableViewController*)controller scrubberColors];
    
    [self updateFromDetailItem];
    [self configureScrubbers];
}


#pragma mark - JWOptionControlsViewControllerDelegate

//func optionsControls(controller:JWOptionControlsViewController, colorForHueDidChange color:UIColor)

-(void)optionsControls:(JWOptionControlsViewController*)controller colorForHueDidChange:(UIColor*)color {
    
    [_scrubberController setBackLightColor:color];
    
    
    id scrubberColors = _detailItem[@"scrubbercolors"];
    
    if (scrubberColors) {
        NSMutableDictionary* modifyColors = scrubberColors;
        modifyColors[JWColorBackgroundHueColor] = color;
        
    } else {
        NSDictionary *modifyColors = @{
                                            JWColorBackgroundHueColor: color
                                            };
        
        _detailItem[@"scrubbercolors"] = [modifyColors mutableCopy];
    }
    
    [_delegate itemChanged:self cachKey:_detailItem[@"key"]];
}

-(void)optionsControls:(JWOptionControlsViewController*)controller colorForTrackDidChange:(UIColor*)color {
    
    id trackColors = _detailItem[@"trackcolors"];
    
    if (trackColors) {
        NSMutableDictionary* modifytrackColors = trackColors;
        modifytrackColors[JWColorScrubberTopPeak] = color;
        [_scrubberController modifyTrack:_scrubberTrackId
                                  colors:modifytrackColors];

    } else {
        NSDictionary *modifytrackColors = @{
                              JWColorScrubberTopPeak: color
                              };
        [_scrubberController modifyTrack:_scrubberTrackId
                                  colors:modifytrackColors];
        
        _detailItem[@"trackcolors"] = [modifytrackColors mutableCopy];
    }
    
}

-(void)optionsControls:(JWOptionControlsViewController*)controller colorForTrackBottomDidChange:(UIColor*)color {
    
    id trackColors = _detailItem[@"trackcolors"];
    
    if (trackColors) {
        NSMutableDictionary* modifytrackColors = trackColors;
        modifytrackColors[JWColorScrubberBottomPeak] = color;
        [_scrubberController modifyTrack:_scrubberTrackId
                                  colors:modifytrackColors];
        
        
    } else {
        NSDictionary *modifytrackColors = @{
                                            JWColorScrubberBottomPeak: color
                                            };
        [_scrubberController modifyTrack:_scrubberTrackId
                                  colors:modifytrackColors];
        
        _detailItem[@"trackcolors"] = [modifytrackColors mutableCopy];
    }
    
}


//    _detailItem[@"trackcolors"]= trackColors;
//
//    [self updateFromDetailItem];
//    [self configureScrubbers];
//
//    [_delegate itemChanged:self cachKey:_detailItem[@"key"]];

#pragma mark -

- (IBAction)buttonPressed:(id)sender {
    if (sender == _playButton) {
        NSLog(@"%s PLAY",__func__);
        _playButton.enabled = NO;
        _pauseButton.enabled = YES;
    } else if (sender == _pauseButton) {
        NSLog(@"%s PAUSE",__func__);
        [_scrubberController modifyTrack:_scrubberTrackId
                                  colors:@{ JWColorScrubberTopPeak : [[UIColor orangeColor] colorWithAlphaComponent:0.6] }];
        _playButton.enabled = YES;
        _pauseButton.enabled = NO;
    } else  if (sender == _rewindButton) {
        NSLog(@"%s REWIND",__func__);
        [_scrubberController modifyTrack:_scrubberTrackId alpha:1.0];
        [_scrubberController modifyTrack:_scrubberTrackId colors:[self defaultWhiteColors]];
    }
}

- (IBAction)refreshAction:(id)sender {
    [self configureScrubbers];
}

- (IBAction)saveAction:(id)sender {
    [_delegate save:self cachKey:_detailItem[@"key"]];
}

- (IBAction)clipAction:(id)sender {
    
    if (self.editing) {
        [self clipActionsEditing];
    } else {
        [self clipActions];
    }
}


-(void)configureScrubberColorsRecordingAudio:(BOOL)recordAudio recordingingMix:(BOOL)recordMix hasMixerPlayer:(BOOL)mixerPlayer {
    
    // Configure Track Colors for all Tracks
    // Override per track if wanted
    // configureColors is whole saled the dictionary is simply kept
    // Whereas per track will interleave with this
    // Configure with RED colors for recording
    if (colorizedTracks == NO) {  // set the base to whites
        // Track colors whiteColor/ whiteColor - WHITE middle
        [_scrubberController configureColors:
         @{
           JWColorScrubberTopPeak : [[UIColor whiteColor] colorWithAlphaComponent:0.6],
           JWColorScrubberTopAvg : [UIColor colorWithWhite:0.9 alpha:0.5] ,
           JWColorScrubberBottomAvg : [UIColor colorWithWhite:0.9 alpha:0.5],
           JWColorScrubberBottomPeak : [[UIColor whiteColor] colorWithAlphaComponent:0.6],
           JWColorScrubberTopPeakNoAvg : [[UIColor whiteColor] colorWithAlphaComponent:0.8],
           JWColorScrubberBottomPeakNoAvg : [[UIColor whiteColor] colorWithAlphaComponent:0.8],
           }
         ];
    }
    
    if (recordMix || recordAudio ) {
        [_scrubberController configureScrubberColors:
         @{ JWColorBackgroundHueColor : [UIColor redColor],
            }];
        // RECORD MIX
        if (recordMix) {
            if (colorizedTracks)
                // Track colors whiteColor/ whiteColor - WHITE middle
                [_scrubberController configureColors:
                 @{
                   JWColorScrubberTopPeak : [[UIColor whiteColor] colorWithAlphaComponent:0.8],
                   JWColorScrubberTopAvg : [UIColor colorWithWhite:0.9 alpha:0.5] ,
                   JWColorScrubberBottomAvg : [UIColor colorWithWhite:0.9 alpha:0.5],
                   JWColorScrubberBottomPeak : [[UIColor whiteColor] colorWithAlphaComponent:0.6],
                   JWColorScrubberTopPeakNoAvg : [[UIColor whiteColor] colorWithAlphaComponent:0.8],
                   JWColorScrubberBottomPeakNoAvg : [[UIColor whiteColor] colorWithAlphaComponent:0.8],
                   }
                 ];
        }
        // RECORDER
        else if (recordAudio) {
            if (colorizedTracks)
                // yellowColor / YELLOW - WHITE middle
                [_scrubberController configureColors:
                 @{
                   JWColorScrubberTopPeak : [[UIColor yellowColor] colorWithAlphaComponent:0.8],
                   JWColorScrubberTopAvg : [UIColor colorWithWhite:0.9 alpha:0.5] ,
                   JWColorScrubberBottomAvg : [UIColor colorWithWhite:0.9 alpha:0.5],
                   JWColorScrubberBottomPeak : [[UIColor yellowColor] colorWithAlphaComponent:0.8],
                   JWColorScrubberTopPeakNoAvg : [[UIColor yellowColor] colorWithAlphaComponent:0.8],
                   JWColorScrubberBottomPeakNoAvg : [[UIColor yellowColor] colorWithAlphaComponent:0.8],
                   }
                 ];
        }
    }
    // ALL OTHERS
    else {
        if (colorizedTracks)
            // Configure with WHITE colors for playing
            [_scrubberController configureColors:
             @{
               JWColorScrubberTopAvg : [UIColor colorWithWhite:0.9 alpha:0.5] ,
               JWColorScrubberTopPeak : [UIColor colorWithWhite:0.7 alpha:0.5],
               JWColorScrubberBottomAvg : [UIColor colorWithWhite:0.7 alpha:0.5],
               JWColorScrubberBottomPeak : [UIColor colorWithWhite:0.9 alpha:0.5],
               JWColorScrubberBottomPeakNoAvg : [UIColor colorWithWhite:0.9 alpha:0.5] ,
               JWColorScrubberTopPeakNoAvg : [UIColor colorWithWhite:0.9 alpha:0.5],
               }
             ];
        
        // MIXER Player
        if (mixerPlayer) {
            [_scrubberController configureScrubberColors:
             @{ JWColorBackgroundHueColor : iosColor1
                }];
        } else {
            [_scrubberController configureScrubberColors:
             @{ JWColorBackgroundHueColor : iosColor2
                }];
        }
    }
    
}


-(NSDictionary*)defaultWhiteColors {
    return @{
             JWColorScrubberTopPeak : [[UIColor whiteColor] colorWithAlphaComponent:0.6],
             JWColorScrubberTopAvg : [UIColor colorWithWhite:0.9 alpha:0.5] ,
             JWColorScrubberBottomAvg : [UIColor colorWithWhite:0.9 alpha:0.5],
             JWColorScrubberBottomPeak : [[UIColor whiteColor] colorWithAlphaComponent:0.6],
             JWColorScrubberTopPeakNoAvg : [[UIColor whiteColor] colorWithAlphaComponent:0.8],
             JWColorScrubberBottomPeakNoAvg : [[UIColor whiteColor] colorWithAlphaComponent:0.8],
             };
}

-(void)configureScrubberColors {
    // Configure Track Colors for all Tracks
    // configureColors is whole saled the dictionary is simply kept
    
//    configureColors
//    configureScrubberColors - needs to be called or crash
    // TODO: fix this
    
    if (colorizedTracks == NO) {  // set the base to whites
        // Track colors whiteColor/ whiteColor - WHITE middle
        [_scrubberController configureColors:[self defaultWhiteColors]];
        [_scrubberController configureScrubberColors:
         @{ JWColorBackgroundHueColor : iosColor2
            }];  // default blue ocean

    } else {
        if ([_scrubberTrackColors count]) {
            [_scrubberController configureColors:_scrubberTrackColors];
            
        } else {
            [_scrubberController configureColors:[self defaultWhiteColors] ];
        }
        
        if ([_scrubberColors count]) {
            [_scrubberController configureScrubberColors:_scrubberColors ];
            
        } else {
            [_scrubberController configureScrubberColors:
             @{ JWColorBackgroundHueColor : iosColor2
                }];  // default blue ocean
        }
    }
}

- (void)configureScrubbers {
    // Update the user interface for the detail item.
    
    BOOL recordAudio;
    BOOL recordMix;
    
    // STEP 2 CONFIGURE SCRUBBER SETTINGS
    [_scrubberController reset];
    
    _scrubberController.useGradient = YES;
    _scrubberController.useTrackGradient = NO;
    
    _scrubberController.numberOfTracks = 1;


    // MULTI DEMO
//    _scrubberController.numberOfTracks = 4;
//    _scrubberController.trackLocations = @[@(0.15),@(0.30)]; // n - 1 locations
//    _scrubberController.numberOfTracks = 4;
//    _scrubberController.trackLocations = @[@(0.15),@(0.30),@(0.450)]; // n - 1 locations

    [self configureScrubberColors];
    [_scrubberController setViewOptions:_viewOptions];
    [self updateScrubberHeight];
    
    SampleSize ssz =  [[_scrubberOptions valueForKey:@"size"] unsignedIntegerValue];
    VABKindOptions kind =  [[_scrubberOptions valueForKey:@"kind"] unsignedIntegerValue];
    VABLayoutOptions layout =  [[_scrubberOptions valueForKey:@"layout"] unsignedIntegerValue];
    
    float delay = 0.0;
    id delayItem = _detailItem[@"starttime"];
    if (delayItem)
        delay = [delayItem floatValue];

    NSDictionary * fileReference;
//    float startInset = [scrubberFileReference[@"startinset"] floatValue];
//    float endInset = [scrubberFileReference[@"endinset"] floatValue];
//    float duration = [scrubberFileReference[@"duration"] floatValue];
//    NSLog(@"%s %.2f %.2f %.2f",__func__,startInset,endInset,duration);
    id referenceFileItem = _detailItem[@"referencefile"];
    if (referenceFileItem)
        fileReference = referenceFileItem;
    
    //trimmedMP3_0CB28386-7EE7-4C7B-9E86-BDCBA0D15FB1
    NSURL *fileURL = [[NSBundle mainBundle] URLForResource:@"trimmedMP3_0CB28386-7EE7-4C7B-9E86-BDCBA0D15FB1" withExtension:@".m4a"];
    
    int index = 0;
    int nodeType = 1;
    // 1 - player
    // 2 - playerRecorder

    // PLAYER
    if (nodeType == 1) {
        if (fileURL) {
            NSLog(@"%s file at index %d\n%@",__func__,index,[fileURL lastPathComponent]);
            // INSIDE  BLUE / BLUE
            _scrubberTrackId =
            [_scrubberController prepareScrubberFileURL:fileURL
                                         withSampleSize:ssz
                                                options:SamplingOptionDualChannel
                                                   type:kind
                                                 layout:layout
                                                 colors:nil
                                          referenceFile:fileReference
                                              startTime:delay
                                           onCompletion:nil];

            // MULTI DEMO
//            for (int i = 0; i<_scrubberController.numberOfTracks -1; i++) {
//                [_scrubberController prepareScrubberFileURL:fileURL
//                                             withSampleSize:ssz
//                                                    options:SamplingOptionDualChannel
//                                                       type:kind
//                                                     layout:layout
//                                                     colors:nil
//                                              referenceFile:fileReference
//                                                  startTime:delay
//                                               onCompletion:nil];
//            }

        } else {
            // no file URL for player
            NSLog(@"%s NO file url at index %d",__func__,index);
        }
        
//        colorizedTracks
//        ? @{
//            JWColorScrubberTopAvg : [[UIColor blueColor] colorWithAlphaComponent:0.8] ,
//            JWColorScrubberBottomAvg : [[UIColor blueColor] colorWithAlphaComponent:0.5],
//            }:nil
    }
    // PLAYER RECORDER
    else if (nodeType == 2) {
        BOOL usePlayerScrubber = YES;  // determine whther to use player or recorder for scrubber
        if (recordAudio) {
            // USE recorder
            usePlayerScrubber = NO;
        } else if (fileURL == nil) {
            usePlayerScrubber = NO;  // not the current recorder - useplayer if has fileURL
        }
        
        NSLog(@"%s use player %@ at index %d",__func__,usePlayerScrubber?@"YES":@"NO",index);
        if (usePlayerScrubber) {
            // PLAYER - YELLOW / YELLOW
            [_scrubberController prepareScrubberFileURL:fileURL
                                         withSampleSize:SampleSize14
                                                options:SamplingOptionDualChannel
                                                   type:VABOptionNone
                                                 layout:VABLayoutOptionOverlayAverages | VABLayoutOptionShowAverageSamples | VABLayoutOptionShowCenterLine
                                                 colors:colorizedTracks
             ? @{
                 JWColorScrubberTopPeak : [[UIColor yellowColor] colorWithAlphaComponent:0.7],
                 JWColorScrubberBottomPeak : [[UIColor yellowColor] colorWithAlphaComponent:0.7],
                 }:nil
                                          referenceFile:fileReference
                                              startTime:delay
                                           onCompletion:nil];
            
        } else {
            if (recordMix == NO) { // uninterested in recorder here only need player for PlayerNodes
                // use recorder
                NSString *recorderTrackId =
                [_scrubberController prepareScrubberListenerSource:nil
                                                    withSampleSize:SampleSize10
                                                           options:SamplingOptionDualChannel
                                                              type:VABOptionCenter
                                                            layout:VABLayoutOptionStackAverages | VABLayoutOptionShowAverageSamples
                                                            colors:colorizedTracks
                 ? @{
                     JWColorScrubberTopPeak : [[UIColor redColor] colorWithAlphaComponent:0.5],
                     JWColorScrubberBottomPeak : [[UIColor yellowColor] colorWithAlphaComponent:0.7],
                     }:nil
                                                      onCompletion:nil];
                
                //                [_audioEngine registerController:_scrubberController withTrackId:recorderTrackId forPlayerRecorderAtIndex:index];
            }
        }
    }
    
    
    [self configureControls];
    
}

-(void)configureControls {
    
    //(id <JWEffectsModifyingProtocol>)
    
    id trackModify  = [_scrubberController trackNodeControllerForTrackId:_scrubberTrackId];
    
    if (_sliderControls) {
        // Volume
        [_sliderControls.sliderControl1 addTarget:trackModify action:@selector(adjustFloatValue1WithSlider:) forControlEvents:UIControlEventValueChanged];
        
        // Pan
        [_sliderControls.sliderControl2 addTarget:trackModify action:@selector(adjustFloatValue2WithSlider:) forControlEvents:UIControlEventValueChanged];
        // Alpha
        [_sliderControls.sliderControl4 addTarget:trackModify action:@selector(adjustFloatValue3WithSlider:) forControlEvents:UIControlEventValueChanged];
        
        // Backlight
        [_sliderControls.sliderControl3 addTarget:_scrubberController action:@selector(adjustFloatValue1WithSlider:) forControlEvents:UIControlEventValueChanged];
        
    } else if (_optionControls) {
        
        // Volume
        [_optionControls.optionSlider1 addTarget:trackModify action:@selector(adjustFloatValue1WithSlider:) forControlEvents:UIControlEventValueChanged];
        // Pan
        [_optionControls.optionSlider2 addTarget:trackModify action:@selector(adjustFloatValue2WithSlider:) forControlEvents:UIControlEventValueChanged];
        // Alpha
        [_optionControls.optionSlider4 addTarget:trackModify action:@selector(adjustFloatValue3WithSlider:) forControlEvents:UIControlEventValueChanged];
        
        // Backlight
        [_optionControls.optionSlider3 addTarget:_scrubberController action:@selector(adjustFloatValue1WithSlider:) forControlEvents:UIControlEventValueChanged];
        
    }
    
    
}


#pragma mark scrubber controler Delegate for buffers

-(CGFloat)progressOfAudioFile:(JWScrubberController*)self forScrubberId:(NSString*)sid{
    return 0.0;
}
-(CGFloat)durationInSecondsOfAudioFile:(JWScrubberController*)self forScrubberId:(NSString*)sid{
    return 0.0;
}
-(CGFloat)remainingDurationInSecondsOfAudioFile:(JWScrubberController*)self forScrubberId:(NSString*)sid{
    return 0.0;
}
-(CGFloat)currentPositionInSecondsOfAudioFile:(JWScrubberController*)self forScrubberId:(NSString*)sid{
    return 0.0;
}
-(NSString*)processingFormatStr:(JWScrubberController*)self forScrubberId:(NSString*)sid{
//    return [_audioEngine processingFormatStr];
    return nil;
}


#pragma mark - edit

-(void)editingMadeChange:(JWScrubberController*)controller forScrubberId:(NSString*)sid {

    [self editingMadeChange:controller forScrubberId:sid withTrackInfo:nil];
}

-(void)editingMadeChange:(JWScrubberController*)controller forScrubberId:(NSString*)sid withTrackInfo:(id)trackInfo {
    
//    NSLog(@"%s %@",__func__,sid);

    self.editChangeTimeStamp = [NSDate date];

    @synchronized(self.currentEditTrackInfo) {
        self.currentEditTrackInfo = trackInfo;
    }

    if ([self editUpdateTask] == NO) {
        
        // DID not update will delay and try again
        double delayInSecs = 0.335;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSecs * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self editUpdateTask];
        });
    }
}


#define TRACEUPDATES

-(BOOL)editUpdateTask {

    NSDate *timeStamp = [NSDate date];
    BOOL doUpdate = NO;
    
    if (_editChangeUpdateTimeStamp == nil){
        doUpdate = YES;
    } else {
        NSTimeInterval timeSinceUpdate = [timeStamp timeIntervalSinceDate:_editChangeUpdateTimeStamp];
        if (timeSinceUpdate > .33)
            doUpdate = YES;
    }
    
    if (doUpdate) {
        self.editChangeUpdateTimeStamp = [NSDate date];
        
#ifdef TRACEUPDATES
        NSLog(@"%s\n=== DO EDIT UPDATE =======================================",__func__);
#endif
        id trackInfo;
        @synchronized(self.currentEditTrackInfo) {
            trackInfo = self.currentEditTrackInfo;
        }
        
        if (trackInfo) {
            if ([trackInfo isKindOfClass:[NSDictionary class]]) {
                // AS DICTIONARY
#ifdef TRACEUPDATES
                id startTimeValue = trackInfo[@"starttime"];
                float startTime = startTimeValue ? [startTimeValue floatValue] : 0.0;
                id refFile = trackInfo[@"referencefile"];
                if (refFile) {
                    id durationValue = refFile[@"duration"];
                    NSTimeInterval durationSeconds  = durationValue ? [durationValue doubleValue] : 0.0;
                    id startInsetValue = refFile[@"startinset"];
                    float startInset = startInsetValue ? [startInsetValue floatValue] : 0.0;
                    id endInsetValue = refFile[@"endinset"];
                    float endInset = endInsetValue ? [endInsetValue floatValue] : 0.0;
                    
                    NSLog(@"%s start %.4f si %.4f ei %.4f dur %.2f\n",__func__,startTime,startInset,endInset,durationSeconds);
                }
      
                // NSTimeInterval frameDurationSeconds =
                
                /*  0000 2267     1/44100
                 
                 
                 */
#endif

            }
            
        } else {
            NSLog(@"%s no reference No change",__func__);
        }

    } else {
        // silently ignore
        //NSLog(@"%s IGNORE update",__func__);
    }
    
    return doUpdate;
    
}

-(void)editingCompleted:(JWScrubberController*)controller forScrubberId:(NSString*)sid {

    [self editingCompleted:controller forScrubberId:sid withTrackInfo:nil];
}

-(void)editingCompleted:(JWScrubberController*)controller forScrubberId:(NSString*)sid withTrackInfo:(id)trackInfo {
   
    if (trackInfo) {
        NSLog(@"%s %@ %@",__func__,sid,[trackInfo description]);

        if ([trackInfo isKindOfClass:[NSDictionary class]]) {
            id startTimeValue = trackInfo[@"starttime"];
            if (startTimeValue)
                _detailItem[@"starttime"] = startTimeValue;
            id refFile = trackInfo[@"referencefile"];
            if (refFile)
                _detailItem[@"referencefile"] = refFile;
            
            [self saveAction:nil];
        }
        
    } else {
        NSLog(@"%s no reference No change %@",__func__,sid);
    }
}

-(void)positionChanged:(JWScrubberController*)controller positionSeconds:(CGFloat)position {

//    NSLog(@"%s %.4f",__func__,position);
    
    self.currentPositionChange = position;
    
    if ([self positionUpdateTask] == NO) {
        // DID not update will delay and try again
        double delayInSecs = 0.335;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSecs * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self positionUpdateTask];
        });
    }
}

-(BOOL)positionUpdateTask {
    
    NSDate *timeStamp = [NSDate date];
    BOOL doUpdate = NO;
    
    if (_positionChangeUpdateTimeStamp == nil){
        doUpdate = YES;
    } else {
        NSTimeInterval timeSinceUpdate = [timeStamp timeIntervalSinceDate:_positionChangeUpdateTimeStamp];
        if (timeSinceUpdate > .33)
            doUpdate = YES;
    }
    
    if (doUpdate) {
        self.positionChangeUpdateTimeStamp = [NSDate date];
        
#ifdef TRACEUPDATES
        NSLog(@"%s\n=== DO POSITION UPDATE =======================================",
              __func__);
        NSLog(@"%s postion %.4f secs",__func__,_currentPositionChange);
#endif

    } else {
        // silently ignore
        //NSLog(@"%s IGNORE update",__func__);
    }
    
    return doUpdate;
}


#pragma mark - view layout

-(void)viewDidLayoutSubviews {
    
    //Get Preview Layer connection
    [self updateScrubberHeight];
}

-(void)updateScrubberHeight{
    if (_scrubberContainer.hidden) {
        return;
    }
    CGFloat tracksz = 60.0f;
    NSUInteger nTracks = _scrubberController.numberOfTracks;
    if (nTracks == 1) {
        tracksz = 120;
    } else if (nTracks == 2) {
        tracksz = 85.0f;
    } else if (nTracks == 3) {
        tracksz = 95.0f;
    }
//    else {
//        tracksz = 45.0f;
//    }
    CGFloat expectedHeight = (_scrubberController.numberOfTracks  * tracksz);// + 40;  // labels on scrubber
    
    self.layoutConstraintScrubberHeight.constant = expectedHeight;

    _scrubberController.scrubberControllerSize = CGSizeMake(self.view.bounds.size.width,self.layoutConstraintScrubberHeight.constant);
}


#pragma mark - clip actions

-(void)clipActions {
    UIAlertController* actionController =
    [UIAlertController alertControllerWithTitle:@"Clip" message:@"Select Action" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* clipLeftTrack =
    [UIAlertAction actionWithTitle:@"Clip Left" style:UIAlertActionStyleDefault handler:^(UIAlertAction* action) {
        self.editing = YES;
        [_scrubberController editTrackBeginInset:_scrubberTrackId];
    }];
    UIAlertAction* clipRightTrack =
    [UIAlertAction actionWithTitle:@"Clip Right" style:UIAlertActionStyleDefault handler:^(UIAlertAction* action) {
        self.editing = YES;
        [_scrubberController editTrackEndInset:_scrubberTrackId];

    }];
    UIAlertAction* startPosition =
    [UIAlertAction actionWithTitle:@"Set Start Position" style:UIAlertActionStyleDefault handler:^(UIAlertAction* action) {
        self.editing = YES;
        [_scrubberController editTrackStartPosition:_scrubberTrackId];
    }];
    UIAlertAction* cancelAction =
    [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction* action) {
    }];
    [actionController addAction:clipLeftTrack];
    [actionController addAction:clipRightTrack];
    [actionController addAction:startPosition];
    [actionController addAction:cancelAction];
    [self presentViewController:actionController animated:YES completion:nil];
    
}

-(void)clipActionsEditing {
    UIAlertController* actionController =
    [UIAlertController alertControllerWithTitle:@"Clip Edit" message:@"Save or Cancel Changes" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* saveClipEdit =
    [UIAlertAction actionWithTitle:@"Save Clip Edit" style:UIAlertActionStyleDefault handler:^(UIAlertAction* action) {
        self.editing = NO;
        [_scrubberController stopEditingTrackSave:_scrubberTrackId];
    }];
    UIAlertAction* cancelClipEdit =
    [UIAlertAction actionWithTitle:@"Cancel Clip Edit" style:UIAlertActionStyleDefault handler:^(UIAlertAction* action) {
        self.editing = NO;
        [_scrubberController stopEditingTrackCancel:_scrubberTrackId];
    }];
    UIAlertAction* cancelAction =
    [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction* action) {
    }];
    
    [actionController addAction:saveClipEdit];
    [actionController addAction:cancelClipEdit];
    [actionController addAction:cancelAction];
    [self presentViewController:actionController animated:YES completion:nil];
    
}


@end


//    if (_editChangeTimeStamp) {
//        timeSinceUpdate = [_editChangeTimeStamp timeIntervalSinceDate:_editChangeUpdateTimeStamp];
//    } else {
//        //_editChangeTimeStamp nil is from delay recall
//        timeSinceUpdate = [timeStamp timeIntervalSinceDate:_editChangeUpdateTimeStamp];
//    }

//    [self configureScrubberColorsRecordingAudio:NO recordingingMix:NO hasMixerPlayer:NO];
//    [_scrubberController configureScrubberColors:_scrubberColors ];
//    [_scrubberController configureColors:_scrubberTrackColors];

//withSampleSize:SampleSize14
//options:SamplingOptionDualChannel
//type:VABOptionNone
//layout:VABLayoutOptionOverlayAverages | VABLayoutOptionShowAverageSamples | VABLayoutOptionShowCenterLine

// perhaps, so long as additional updates do not come in

//        self.editChangeTimeStamp = nil;
//        NSLog(@"%s DELAY update",__func__);

//    if ([self editUpdateTask:trackInfo] == NO) {
//        // DID not update will delay and try again
//        // perhaps, so long as additional updates do not come in
////        self.editChangeTimeStamp = nil;
////        NSLog(@"%s DELAY update",__func__);
//        double delayInSecs = 0.335;
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSecs * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            [self editUpdateTask:trackInfo];
//        });
//    }

//    _detailItem[@"viewoptions"] = @(controller.viewOptions);
//    _detailItem[@"options"] = controller.optionsData;
//    _detailItem[@"trackcolors"] = controller.scrubberTrackColors;
//    _detailItem[@"scrubbercolors"] = controller.scrubberColors;
//    [self updateFromDetailItem];
//    [self configureScrubbers];
////    [_delegate itemChanged:self];
//    [_delegate itemChanged:self cachKey:_detailItem[@"key"]];


